﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;

public class TerrainChunkTiletype_BaseTile {
	//--Load-in parts
	string name;
	int designSeed;
	string texturePath;
	int materialType;

	Color biomeInputColor;
	Color hexagonVertexColorMult;
	float hexagonVertexColorNoise;

	int tileRes;
	int textureRes;

	int tileVariationCount;
	int orderHeightIndex;
	float tileMeshScale;
	List<int> variationList;

	

	List<List<Vector2>> tileVariationList = new List<List<Vector2>>(); //These store the tile UV positions


	
	public string Name { get => name; set => name = value; }
	public string TexturePath { get => texturePath; set => texturePath = value; }
	public int MaterialType { get => materialType; set => materialType = value; }
	public int TileRes { get => tileRes; set => tileRes = value; }
	public int TextureRes { get => textureRes; set => textureRes = value; }

	public int TileVariationCount {
		get => tileVariationCount;
		set => tileVariationCount = value;
	}


	public int OrderHeightIndex { get => orderHeightIndex; set => orderHeightIndex = value; }
	public float TileMeshScale { get => tileMeshScale; set => tileMeshScale = value; }
	

	 
	public int DesignSeed { get => designSeed; set => designSeed = value; }
	public Color BiomeInputColor { get => biomeInputColor; set => biomeInputColor = value; }
	public Color HexagonVertexColorMult { get => hexagonVertexColorMult * 255; set => hexagonVertexColorMult = value; }
	public float HexagonVertexColorNoise { get => hexagonVertexColorNoise * 255; set => hexagonVertexColorNoise = value; }

	////public int TileVariation { get => tileVariation;
	//	set {
	//		//If list doesn't exist, make it.
	//		Debug.Log( "Dah!  Value : " + value );
	//		if ( VariationList == null ) { VariationList = new List<int>( ); }
	//		VariationList.Add( value );
	//	}
	//}
	[XmlElement( "TileVariation" )]
	public List<int> VariationList { get => variationList; set => variationList = value; }
	 
	public TerrainChunkTiletype_BaseTile ()
	{
		Name = "";
		TexturePath = "";
		MaterialType = -1;
		TileRes = -1;
		TileVariationCount = -1;
		OrderHeightIndex = -1;
		TileMeshScale = -1;
		VariationList = new List<int>( );
	}
	
}
