﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 Creates a single hexagon mesh class.
 * */
class HexagonMeshGenerator {
	//--Assists in the shape of the hexagon, for oddly shaped ones.  Sides are pointed.  
	float topVerticeOffsetDistance;
	float topVerticalHorizontalOffsetDistance;
	float sideVerticeOffsetDistance;
	float bottomVerticeOffsetDistance;
	float bottomVerticalHorizontalOffsetDistance;

	//--Initalizes points with distance. Used for basic mesh shape.
	public void SetVertexPointsDistance ( float td , float ta , float sd , float bd , float ba )
	{
		topVerticeOffsetDistance = Mathf.Abs( td );
		topVerticalHorizontalOffsetDistance = Mathf.Abs( ta );
		sideVerticeOffsetDistance = Mathf.Abs( sd );
		bottomVerticeOffsetDistance = Mathf.Abs( bd );
		bottomVerticalHorizontalOffsetDistance = Mathf.Abs( ba );
	}

	//Returns a mesh component with assigned data
	public void CreateMeshComponent ( Mesh inputMeshComponent )
	{
		//Assign specific things here.
		inputMeshComponent = CreateMeshComponent( );
	}
	public Mesh CreateMeshComponent ()
	{
		Mesh returnMesh = new Mesh();
		//--Get vertices
		returnMesh.vertices = GetHexagonVertices( );
		returnMesh.triangles = GetHexagonTriangles( );
		returnMesh.normals = GetHexagonNormals( returnMesh.vertices );
		returnMesh.uv = GetHexagonUVs( returnMesh.vertices );

		returnMesh.RecalculateNormals( );
		returnMesh.RecalculateTangents( );
		returnMesh.RecalculateBounds( );

		return returnMesh;
	}
	//Returns an array of size 6.   
	 Vector3[] GetHexagonVertices ()
	{
		//		p0	p1
		//	p2	  [c]	p3
		//		p4	p5
		Vector3[] meshVertices = new Vector3[6];

		meshVertices[0] = new Vector3( -topVerticalHorizontalOffsetDistance , topVerticeOffsetDistance , 0.0f );
		meshVertices[1] = new Vector3( topVerticalHorizontalOffsetDistance , topVerticeOffsetDistance , 0.0f );

		meshVertices[2] = new Vector3( -sideVerticeOffsetDistance , 0.0f , 0.0f );
		meshVertices[3] = new Vector3( sideVerticeOffsetDistance , 0.0f , 0.0f );

		meshVertices[4] = new Vector3( -bottomVerticalHorizontalOffsetDistance , -bottomVerticeOffsetDistance , 0.0f );
		meshVertices[5] = new Vector3( bottomVerticalHorizontalOffsetDistance , -bottomVerticeOffsetDistance , 0.0f );

		return meshVertices;
	}

	//Returns a triangle list.  Assumes 6 vertices.  Horizontal side points are their own triangle, leaving 2 triangles in the middle.
	 int[] GetHexagonTriangles ()
	{
		int[] meshTriangles = new int[12];
		//-Triangle 1
		meshTriangles[0] = 0;
		meshTriangles[1] = 4;
		meshTriangles[2] = 2;

		//-Triangle 2
		meshTriangles[3] =4;
		meshTriangles[4] = 0;
		meshTriangles[5] = 5;


		//-Triangle 3
		meshTriangles[6] = 0;
		meshTriangles[7] = 1;
		meshTriangles[8] = 5;


		//-Triangle 4
		meshTriangles[9] = 1;
		meshTriangles[10] = 3;
		meshTriangles[11] = 5;

		return meshTriangles;
	}

	 Vector3[] GetHexagonNormals (Vector3[] meshVertices)
	{
		Vector3[] meshNormals = new Vector3[6];
		for (int i = 0 ; i < 6 ; ++i ) {
		//	meshNormals[i] = Vector3.up;
		}
		meshNormals[0] = VertexNormal( 0 , 4 , 2 , meshVertices );
		meshNormals[1] = VertexNormal( 0 , 4 , 5 , meshVertices );
		meshNormals[2] = VertexNormal( 0 , 1 , 5 , meshVertices );
		meshNormals[3] = VertexNormal( 1 , 3 , 5 , meshVertices );
		return meshNormals;
	}

	Vector2[] GetHexagonUVs (Vector3[] meshVertices )
	{
		Vector2[] meshUV = new Vector2[6];
		Vector2 minDist = new Vector2(100,100);
		Vector2 maxDist = new Vector2(-100,-100);
		//--Find min, max distances.  
		foreach (Vector3 vertex in meshVertices ) {
			if (vertex.x < minDist.x ) { minDist.x = vertex.x; }
			if ( vertex.y < minDist.y ) { minDist.y = vertex.y; }

			if ( vertex.x > maxDist.x ) { maxDist.x = vertex.x; }
			if ( vertex.y > maxDist.y ) { maxDist.y = vertex.y; }
		}
		float horizontalDist = maxDist.x - minDist.x;
		float verticalDist = maxDist.y - minDist.y;
		for (int i= 0 ; i < 6 ; ++i ) {
			meshUV[i] = new Vector2( (meshVertices[i].x - minDist.x) / horizontalDist, ( meshVertices[i].y - minDist.y ) / verticalDist);
		}
		return meshUV;
	}

	static Vector3 VertexNormal ( int vertA , int vertB , int vertC, Vector3[] meshVertices  )
	{
		//b-a , c-a
		return Vector3.Normalize( Vector3.Cross( meshVertices[vertB] - meshVertices[vertA] , meshVertices[vertC] - meshVertices[vertA] ) );
	}
}
