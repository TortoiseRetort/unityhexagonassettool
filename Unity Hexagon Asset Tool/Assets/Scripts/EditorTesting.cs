﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditorTesting : MonoBehaviour {
	public MeshFilter editMesh;
	public float topVerticeOffsetDistance = 1.0f;
	public float topVerticalHorizontalOffsetDistance = 1.0f;
	public float sideVerticeOffsetDistance = 1.0f;
	public float bottomVerticeOffsetDistance = 1.0f;
	public float bottomVerticalHorizontalOffsetDistance = 1.0f;
	HexagonMeshGenerator meshGen = new HexagonMeshGenerator();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (editMesh == null ) { return;}
		meshGen.SetVertexPointsDistance( topVerticeOffsetDistance , topVerticalHorizontalOffsetDistance , sideVerticeOffsetDistance , bottomVerticeOffsetDistance , bottomVerticalHorizontalOffsetDistance );
		editMesh.mesh = meshGen.CreateMeshComponent( );
	}
}
