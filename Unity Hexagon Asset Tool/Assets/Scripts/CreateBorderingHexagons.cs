﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CreateBorderingHexagons : MonoBehaviour {
	public bool forceRegen;
	public Transform baseTransform;
	List<Transform> neighborList;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (baseTransform == null ) { return; }
		if (forceRegen == false ) { return;}
		forceRegen = false;
		if (neighborList != null ) {
			for (int i =0 ; i < neighborList.Count ; ++i ) {
				if (neighborList[i] != null ) { DestroyImmediate( neighborList[i].gameObject ); }
			}
		}
		neighborList = new List<Transform>( );
		baseTransform.gameObject.SetActive( false );
		//base
		Transform newObject = Instantiate(baseTransform);
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( 0 , 0 , 0 );
		newObject.gameObject.SetActive( true );
		neighborList.Add( newObject );
		//--
		newObject = Instantiate(baseTransform);
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( 0 , 0 , 1 );
		newObject.gameObject.SetActive( true );

		neighborList.Add( newObject );

		newObject = Instantiate( baseTransform );
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( 0, 0 , -1 );
		newObject.gameObject.SetActive( true );

		neighborList.Add( newObject );

		newObject = Instantiate( baseTransform );
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( 1 , 0 , 0.5f );
		newObject.gameObject.SetActive( true );

		neighborList.Add( newObject );

		newObject = Instantiate( baseTransform );
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( 1 , 0 , -0.5f );
		newObject.gameObject.SetActive( true );

		neighborList.Add( newObject );

		newObject = Instantiate( baseTransform );
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( -1 , 0 , 0.5f );
		newObject.gameObject.SetActive( true );

		neighborList.Add( newObject );

		newObject = Instantiate( baseTransform );
		newObject.parent = transform;
		newObject.gameObject.SetActive( true );
		newObject.localPosition = new Vector3( -1 , 0 , -0.5f );
		newObject.gameObject.SetActive( true );

		neighborList.Add( newObject );

		foreach (Transform neighbor in neighborList ) {
			UpdateNeighbor( neighbor );
		}
	}

	void UpdateNeighbor(Transform t )
	{
		t.GetComponent<PopulateHexagon>( ).RefreshObject( );
	}
}
