﻿using UnityEngine;

public class OnSpawn_MoveForward : MonoBehaviour {
	public bool initialRotation_Random = false;
	public bool initialRotation_TowardsCenterHexagon = false;
	public bool initialRotation_AwayFromCenterHexagon= false;


	public bool tickRotation_Constant = false;
	float randRotationS = 0.0f;
	float randRotationF = 0.0f;
	public float successForwardStep = 0.01f;
	public float successRotateAmountMin = 1.0f; //How much to rotate on each spawn
	public float successRotateAmountMax = 2.0f; //How much to rotate on each spawn

	public float failureForwardStep = 0.01f;
	public float failureRotateAmountMin = 2.0f; //How much to rotate on each spawn
	public float failureRotateAmountMax = 2.0f; //How much to rotate on each spawn


	public void OnObjectSpawn ( Transform hexagonCenter )
	{
		if ( initialRotation_Random ) {
			transform.Rotate( new Vector3( 0 , Random.Range( 0.0f , 360.0f ) , 0 ) );
			return;
		}
		if ( initialRotation_TowardsCenterHexagon ) {
			transform.LookAt( hexagonCenter.position , new Vector3( 0 , 1 , 0 ) );
		}
		if ( initialRotation_AwayFromCenterHexagon ) {
			transform.LookAt( hexagonCenter.position , new Vector3( 0 , 1 , 0 ) );
			transform.Rotate( new Vector3( 0 , 180.0f , 0 ) );
		}

		if ( tickRotation_Constant ) {
			if (Random.value >= 0.5f ) {
				randRotationS = Random.Range( successRotateAmountMin , successRotateAmountMax );
				randRotationF = Random.Range( failureRotateAmountMin , -failureRotateAmountMax );
			}
			else {
				randRotationS = -Random.Range( successRotateAmountMin , successRotateAmountMax );
				randRotationF = -Random.Range( failureRotateAmountMin , -failureRotateAmountMax );
			}
		
		}
		
	}

	public void DebugTest ( Transform hexagonCenter )
	{
		Debug.Log( "Unit - " + transform.name + "  unit.parent - " + transform.parent.name );
		Debug.Log( "    " + "Position : " + transform.position + "  local position : " + transform.localPosition );
		Debug.Log( "    " + "Parent position : " + transform.parent.position + "  local parentposition : " + transform.parent.localPosition );
		Debug.Log( "    " + "Rotation : " + transform.eulerAngles + "  local rotation : " + transform.localEulerAngles );
		Debug.Log( "    " + "forward - " + transform.forward );
		Quaternion initialRotaiton = transform.rotation;

		//transform.LookAt( hexagonCenter.position , new Vector3( 0 , 0, 0 ) );
		//Debug.Log( "    " + "0 0 0 Rotation : " + transform.eulerAngles + "  local rotation : " + transform.localEulerAngles );
		//Debug.Log( "        " + "forward - " + transform.forward );
		//Debug.Log( "        " + "up - " + transform.up );

		//transform.rotation = initialRotaiton;
		//transform.LookAt( hexagonCenter.position , new Vector3( 1 , 0 , 0 ) );
		//Debug.Log( "    " + "1 0 0 Rotation : " + transform.eulerAngles + "  local rotation : " + transform.localEulerAngles );
		//Debug.Log( "        " + "forward - " + transform.forward );
		//Debug.Log( "        " + "up - " + transform.up );
		//transform.rotation = initialRotaiton;

		//transform.LookAt( hexagonCenter.position , new Vector3( 0 , 1 , 0 ) );
		//Debug.Log( "    " + "0 1 0 Rotation : " + transform.eulerAngles + "  local rotation : " + transform.localEulerAngles );
		//Debug.Log( "        " + "forward - " + transform.forward );
		//Debug.Log( "        " + "up - " + transform.up );
		//transform.rotation = initialRotaiton;

		//transform.LookAt( hexagonCenter.position , new Vector3( 0 , 0 , 1 ) );
		//Debug.Log( "    " + "0 0 1 Rotation : " + transform.eulerAngles + "  local rotation : " + transform.localEulerAngles );
		//Debug.Log( "        " + "forward - " + transform.forward );
		//Debug.Log( "        " + "up - " + transform.up );

	}
	public void OnObjectSpawnSuccess ()
	{
		if ( tickRotation_Constant ) {
			if ( randRotationS < 0 ) {
				transform.Rotate( new Vector3( 0 , Random.Range( -randRotationS , -successRotateAmountMin ) , 0 ) );

			}
			else {
				transform.Rotate( new Vector3( 0 , Random.Range( successRotateAmountMin , randRotationS ) , 0 ) );

			}

		}
		else {
			 
			if ( Random.value >= 0.5f ) {
				transform.Rotate( new Vector3( 0 , Random.Range( successRotateAmountMin , successRotateAmountMax ) , 0 ) );
			}
			else {
				transform.Rotate( new Vector3( 0 , -Random.Range( successRotateAmountMin , successRotateAmountMax ) , 0 ) );
			}

		}
		transform.position += ( transform.forward * successForwardStep );

	}

	public void OnObjectSpawnFailure ()
	{
		if ( tickRotation_Constant ) {
			if ( randRotationF < 0 ) {
				transform.Rotate( new Vector3( 0 , Random.Range( -randRotationF , -failureRotateAmountMin ) , 0 ) );

			}
			else {
				transform.Rotate( new Vector3( 0 , Random.Range( failureRotateAmountMin , randRotationF ) , 0 ) );

			}

		}
		else {
			if ( Random.value >= 0.5f ) {
				transform.Rotate( new Vector3( 0 , Random.Range( failureRotateAmountMin , failureRotateAmountMax ) , 0 ) );
			}
			else {
				transform.Rotate( new Vector3( 0 , -Random.Range( failureRotateAmountMin , failureRotateAmountMax ) , 0 ) );
			}
		}
		transform.position += ( transform.forward * failureForwardStep );
	}
}
