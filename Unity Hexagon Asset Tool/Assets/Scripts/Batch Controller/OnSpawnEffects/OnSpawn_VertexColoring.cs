﻿using UnityEngine;


[ExecuteInEditMode]
public class OnSpawn_VertexColoring : MonoBehaviour {
	public Color minColor = Color.white;
	public Color maxColor = Color.white;

	public void SpawnSuccess ()
	{
		Mesh objectMesh = null;
		Color setColor = Color.Lerp(minColor, maxColor, Random.value);


		if (GetComponent<MeshRenderer>() != null ) {
			GetComponent<MeshRenderer>( ).material.color = setColor;

		}
	}
	


}
