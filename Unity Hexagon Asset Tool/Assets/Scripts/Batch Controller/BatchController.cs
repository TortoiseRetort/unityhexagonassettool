﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;
/*
 Handles generating the entire file.

Create a way to spawn a set number of things, populate it, disable the end pieces, take a picture, delete. 


	Hexagons now spawn with correct UV.  Next step..
		Decide on how artstle will progress.
			currently thinking each biome (ground) has own map, with other things like forests being overlayed on top.
				each terrain type still occupies it's own hexagon, but now eighboring hexagons get some of that style.
				As long as the 'full' base tile is complete, there should be no transparency issues.
				Terrain types have a layer ordering.  A desert goes under a mountain tile .
		OCEAN
			I think there are two ways to approach it.  The ocean itself is the border tile, land goes from 100% to nothing (or mabe still edge tiles)
			way 1 : Ocean covers land.  This enables the greatest variation possible.
			way 2 : Land covers ocean.  I'm not sure about this way, feels like more work for not much gain.  
			Either way allows for a clear terrain edge.


 */
[ExecuteInEditMode]
public class BatchController : MonoBehaviour {
	public bool generateTextures = false;
	public bool abortGenerate = false;
	public int seed = 1;
	public int baseStyleGen = 1; //What active piece are working on

	public int tileResolution = 128;
	public int textureResolution = 4096;
	public int forceVariationLimit = 0; //If 0 or less than, there is no limit to the variations.
	//public bool force1Var = false;
	public bool forceFull = false;
	public int[] variationRatios = new int[7];
	//These are ordered in number of blank neighbors.  so 0 is fully surrounded, 1 has 5 full tiles.  6 is full tiles.

	public bool randSeed = false;


	public Transform baseShapePrefab; //Contains the most basic shape. 
	public Transform spawnerHolder;
	public Camera alphaRenderCamera; //Used to render our shapes
	public Camera colorRenderCamera; //Used to render our shapes
	GameObject spawnedObjectsParent;

	public LayerMask hexagonMask;
	public LayerMask regularMask;
	public LayerMask noSpawnMask;
	//--Allows better configeration with effects and aliasing
	public bool destroyAfterComplete = false;

	public List<BiomeBase> biomeBaseList;
	public BiomeBase specialEmpty_NoSpawnSides;
	public BiomeBase specialEmpty_NoSpawnTop;

	int failCounter = 0;
	int failMax = 5;

	public RandomMaterialAssign materialAssign;

	//variationRatios
	int[] variationsPerType ;
	int[] uniqueTypesPer;
	//Calculates how many tiles per type we should make to fit within the texure resolution.

	void CalculateAllowedNeighbors ()
	{
		variationsPerType = new int[7]; //Index of how many variatios we want per. This is before scaling to fit the full resolution given.
		uniqueTypesPer = new int[7]; //Index of how many variations are possible per each type.  B

		int maxTiles = textureResolution/ tileResolution;
		maxTiles = maxTiles * maxTiles;

		int totalVariations  = 0;
		uniqueTypesPer[6] = 1;
		variationsPerType[6] = uniqueTypesPer[6] * variationRatios[6];
		totalVariations = variationsPerType[6];
		for ( int i = 0 ; i < 6 ; ++i ) {
			uniqueTypesPer[i] = Factorial( 6 ) / ( Factorial( i ) * Factorial( 6 - i ) );
			variationsPerType[i] = uniqueTypesPer[i] * variationRatios[i];
			totalVariations += variationsPerType[i];
			//	Debug.Log( "Types per " + i + "  " + uniqueTypesPer[i] +  "  total variations per type : " + variationsPerType[i]);
		}
		//Debug.Log( "Total variations : " + totalVariations );
		//This gives me a ratio to work with
		//Debug.Log( "Ratio : " + ( maxTiles / totalVariations ) );
		float ratioMult = maxTiles / (totalVariations*1.0f);
		for ( int i = 0 ; i < 7 ; ++i ) {
			variationsPerType[i] = ( int )( ratioMult * variationsPerType[i] );

			//	Debug.Log( "adjusted variation : " + i + "  " + variationsPerType[i] );
			//	Debug.Log( "Tiles per variation : " + i + "  " + variationsPerType[i] / uniqueTypesPer[i] );//
		}



	}
	int Factorial ( int f )
	{
		int r = 1;
		//Debug.Log( "Factiorial for f " + f );
		for ( int i = 0 ; i <= f ; ++i ) {
			if ( i == 0 ) { r = 1; }
			else { r *= i; }
		}

		return r;
	}
	private void Update ()
	{
		if ( generateTextures ) {
			generateTextures = false;
			StartCoroutine( "BatchRender64" );
		}

	}

	public IEnumerator BatchRender64 ()
	{

		//Tiles to generate will need to be include all variation counts as a sum.  
		Debug.Log( "Beginning... " );

		CalculateAllowedNeighbors( );
		TextureAtlasGenerator atlasTexture = new TextureAtlasGenerator(textureResolution, tileResolution,  biomeBaseList[baseStyleGen].biomeName );
		int baseType = 0; //Current baseType of what we are generating.

		Texture2D tempTexture = null;
		materialAssign.CleanMaterialList64( );
		if ( baseShapePrefab == null ) {
			Debug.Log( "Error : No base shape prefab" ); yield return null;

		}
		if ( alphaRenderCamera == null || colorRenderCamera == null ) {
			Debug.Log( "Error : No render camera prefab" ); yield return null;

		}
		if ( randSeed ) { seed = Random.Range( 1 , 10000000 ); }
		Random.InitState( seed );
		double timeToRender = EditorApplication.timeSinceStartup;

		float timeForYield = 0.05f;
		float timeAtLastYield = 0.0f;

		List<BiomeSpawnerInformation> spawnList = new List<BiomeSpawnerInformation>();
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		spawnList.Add( biomeBaseList[baseStyleGen].basicTile );
		int seedCounter = 0;

		/*
		000000 0 - neighbor,s starting at top of hexagon and moving CCW
		111111 0 - surrounded, empty inside
		111111 1 - surrounded, filled inside
		010000 0 - top-left filled, all else empty
		xxxxxx 1  X's don't matter, will automaticlaly be 1.  Impossible to have 101111 1  

		Starts at 000000 0, and begins to count upwards.   000001 0 -> 000010 0 ect
	*/
		int variationsToMake = 0;
		int emptyNeighborCount = 0;
		TerrainChunkTiletype_BaseTile tileTypeInfo = new TerrainChunkTiletype_BaseTile();

		int tilesMade = 0;
		int maxTiles = textureResolution/ tileResolution;
		maxTiles = maxTiles * maxTiles;
		if ( forceFull != true ) {
			for ( int a0 = 0 ; a0 < 2 ; ++a0 ) {
				for ( int a1 = 0 ; a1 < 2 ; ++a1 ) {
					for ( int a2 = 0 ; a2 < 2 ; ++a2 ) {
						for ( int a3 = 0 ; a3 < 2 ; ++a3 ) {
							for ( int a4 = 0 ; a4 < 2 ; ++a4 ) {
								for ( int a5 = 0 ; a5 < 2 ; ++a5 ) {
									// a5 a4 a3 a2 a1 a0   is xxxxxx  Final bit is hanlded seperately

									emptyNeighborCount = 0;
									spawnList[0] = biomeBaseList[0].basicTile;

									if ( a5 == 0 ) {
										if (biomeBaseList[baseStyleGen].useSpecialEmptyTile == true ) {
											spawnList[1] = biomeBaseList[baseStyleGen].emptySpecialTile;

										}
										else {
											spawnList[1] = biomeBaseList[0].basicTile;
										}

										if (biomeBaseList[baseStyleGen].useSpecialNoSpawnSides ) {
											spawnList[1] = specialEmpty_NoSpawnTop.basicTile;
										}
										emptyNeighborCount++;
									}
									else { spawnList[1] = biomeBaseList[baseStyleGen].basicTile; }

									if ( a4 == 0 ) {
										//spawnList[6] = biomeBaseList[0].basicTile;
										if ( biomeBaseList[baseStyleGen].useSpecialEmptyTile == true ) {
											spawnList[6] = biomeBaseList[baseStyleGen].emptySpecialTile;
										}
										else {
											spawnList[6] = biomeBaseList[0].basicTile;
										}

										if ( biomeBaseList[baseStyleGen].useSpecialNoSpawnSides ) {
											spawnList[6] = specialEmpty_NoSpawnSides.basicTile;
										}

										emptyNeighborCount++; }
									else { spawnList[6] = biomeBaseList[baseStyleGen].basicTile; }

									if ( a3 == 0 ) {
										//spawnList[5] = biomeBaseList[0].basicTile;
										if ( biomeBaseList[baseStyleGen].useSpecialEmptyTile == true ) {
											spawnList[5] = biomeBaseList[baseStyleGen].emptySpecialTile;
										}
										else {
											spawnList[5] = biomeBaseList[0].basicTile;
										}


										if ( biomeBaseList[baseStyleGen].useSpecialNoSpawnSides ) {
											spawnList[5] = specialEmpty_NoSpawnSides.basicTile;
										}

										emptyNeighborCount++; }
									else { spawnList[5] = biomeBaseList[baseStyleGen].basicTile; }

									if ( a2 == 0 ) {
										//spawnList[4] = biomeBaseList[0].basicTile;
										if ( biomeBaseList[baseStyleGen].useSpecialEmptyTile == true ) {
											spawnList[4] = biomeBaseList[baseStyleGen].emptySpecialTile;

										}
										else {
											spawnList[4] = biomeBaseList[0].basicTile;
										}


										if ( biomeBaseList[baseStyleGen].useSpecialNoSpawnSides ) {
											spawnList[4] = specialEmpty_NoSpawnTop.basicTile;
										}
										emptyNeighborCount++; }
									else { spawnList[4] = biomeBaseList[baseStyleGen].basicTile; }

									if ( a1 == 0 ) {
										//spawnList[3] = biomeBaseList[0].basicTile;
										if ( biomeBaseList[baseStyleGen].useSpecialEmptyTile == true ) {
											spawnList[3] = biomeBaseList[baseStyleGen].emptySpecialTile;

										}
										else {
											spawnList[3] = biomeBaseList[0].basicTile;
										}


										if ( biomeBaseList[baseStyleGen].useSpecialNoSpawnSides ) {
											spawnList[3] = specialEmpty_NoSpawnSides.basicTile;
										}

										emptyNeighborCount++; }
									else { spawnList[3] = biomeBaseList[baseStyleGen].basicTile; }

									if ( a0 == 0 ) {
									//	spawnList[2] = biomeBaseList[0].basicTile;
										if ( biomeBaseList[baseStyleGen].useSpecialEmptyTile == true ) {
											spawnList[2] = biomeBaseList[baseStyleGen].emptySpecialTile;

										}
										else {
											spawnList[2] = biomeBaseList[0].basicTile;
										}


										if ( biomeBaseList[baseStyleGen].useSpecialNoSpawnSides ) {
											//Debug.Log( "DAH" );
											spawnList[2] = specialEmpty_NoSpawnSides.basicTile;
										}
										emptyNeighborCount++; }
									else { spawnList[2] = biomeBaseList[baseStyleGen].basicTile; }

									//Make variations here
									if ( emptyNeighborCount == 6 ) { variationsToMake = 1; }
									else {
										variationsToMake = variationsPerType[emptyNeighborCount] / uniqueTypesPer[emptyNeighborCount];
									}
									variationsToMake = Mathf.Max( variationsToMake , 1 );
									if ( forceVariationLimit  > 0 ) {
										variationsToMake = Mathf.Clamp( variationsToMake , 1 , forceVariationLimit );
									}
									//if ( force1Var ) { variationsToMake = 1; }
									if ( abortGenerate ) { variationsToMake = 0; }
									tileTypeInfo.VariationList.Add( variationsToMake );
									for ( int v = 0 ; v < variationsToMake ; ++v ) {
										Debug.Log( "Batch  " + baseType + "  || " + ( v / ( variationsToMake * 1.0f ) * 100 ) + "%" + "  " + ( EditorApplication.timeSinceStartup - timeToRender ) );
										tempTexture = GetSingleBatchTexture( seed + seedCounter ,
										spawnList[0] ,  //Center
										spawnList[1] ,  //Top 
										spawnList[2] ,  //Top left
										spawnList[3] ,  //Bottom left
										spawnList[4] ,  //Bottom
										spawnList[5] ,  //Bottom right
										spawnList[6] ); //Top right

										seedCounter++;
										tilesMade++;
										atlasTexture.AddTextureToAtlas( baseType , tempTexture );

										if ( EditorApplication.timeSinceStartup - timeAtLastYield > timeForYield ) {
											yield return null;
											timeAtLastYield = ( float )EditorApplication.timeSinceStartup;
										}

									}
									baseType++; //?
								}
							}
						}
					}
				}
			}
		}
		//Final full scale here
		variationsToMake = maxTiles - tilesMade;
		if ( forceFull ) { variationsToMake = 10; baseType = 64; }
		if ( forceVariationLimit > 0 ) {
			variationsToMake = Mathf.Clamp( variationsToMake , 1 , forceVariationLimit );
		}
		//	if ( force1Var ) { variationsToMake = 1; }
		if ( abortGenerate ) { variationsToMake = 0; }

		spawnList[0] = biomeBaseList[baseStyleGen].basicTile;
		spawnList[1] = biomeBaseList[baseStyleGen].basicTile;
		spawnList[2] = biomeBaseList[baseStyleGen].basicTile;
		spawnList[3] = biomeBaseList[baseStyleGen].basicTile;
		spawnList[4] = biomeBaseList[baseStyleGen].basicTile;
		spawnList[5] = biomeBaseList[baseStyleGen].basicTile;
		spawnList[6] = biomeBaseList[baseStyleGen].basicTile;

		if ( biomeBaseList[baseStyleGen].useSpecialCenterTile == true ) {

			spawnList[0] = biomeBaseList[baseStyleGen].centerSpecialTile;
			if ( biomeBaseList[baseStyleGen].useNoNeighborsWithSpecial == true ) {
				spawnList[1] = biomeBaseList[0].basicTile;
				spawnList[2] = biomeBaseList[0].basicTile;
				spawnList[3] = biomeBaseList[0].basicTile;
				spawnList[4] = biomeBaseList[0].basicTile;
				spawnList[5] = biomeBaseList[0].basicTile;
				spawnList[6] = biomeBaseList[0].basicTile;
			}
		}

		tileTypeInfo.VariationList.Add( variationsToMake );
		for ( int v = 0 ; v < variationsToMake ; ++v ) {
			Debug.Log( "Batch  " + 64 + "  || " + ( v / ( variationsToMake * 1.0f ) * 100 ) + "%" + "  " + ( EditorApplication.timeSinceStartup - timeToRender ) );

			tempTexture = GetSingleBatchTexture( seed + seedCounter ,
			spawnList[0] ,  //Center
			spawnList[1] ,  //Top 
			spawnList[2] ,  //Top left
			spawnList[3] ,  //Bottom left
			spawnList[4] ,  //Bottom
			spawnList[5] ,  //Bottom right
			spawnList[6] ); //Top right

			seedCounter++;

			atlasTexture.AddTextureToAtlas( baseType , tempTexture );
			if ( EditorApplication.timeSinceStartup - timeAtLastYield > timeForYield ) {
				yield return null;
				timeAtLastYield = ( float )EditorApplication.timeSinceStartup;
			}

		}
		baseType++; //?
		Debug.Log( "WRITING TO FILES " + ( EditorApplication.timeSinceStartup - timeToRender ) );

		tileTypeInfo.Name = biomeBaseList[baseStyleGen].biomeName;
		tileTypeInfo.TexturePath = "/Config/TerrainTextures/" + biomeBaseList[baseStyleGen].biomeName + ".png";
		tileTypeInfo.MaterialType = biomeBaseList[baseStyleGen].materialType;

		tileTypeInfo.TileRes = tileResolution;
		tileTypeInfo.TileVariationCount = baseType;
		tileTypeInfo.OrderHeightIndex = biomeBaseList[baseStyleGen].orderHeightIndex;
		tileTypeInfo.TileMeshScale = biomeBaseList[baseStyleGen].tileMeshScale;

		tileTypeInfo.BiomeInputColor = biomeBaseList[baseStyleGen].biomeInputColor;
		tileTypeInfo.HexagonVertexColorMult = biomeBaseList[baseStyleGen].hexagonVertexColor;
		tileTypeInfo.HexagonVertexColorNoise = biomeBaseList[baseStyleGen].hexagonVertexColorNoise;
		tileTypeInfo.TextureRes = textureResolution;
		tileTypeInfo.DesignSeed = seed;
		string fullPath = "Assets/TextureExports/" + tileTypeInfo.Name + ".ChunkTileType";

		XmlSerializer serializer =  new XmlSerializer(typeof(TerrainChunkTiletype_BaseTile));
		Stream fs = new FileStream(@fullPath, FileMode.Create);
		//--
		XmlTextWriter tw = new XmlTextWriter(fs,Encoding.Unicode);
		tw.Formatting = Formatting.Indented;
		tw.Indentation = 4;

		XmlSerializer ser = new XmlSerializer(typeof(TerrainChunkTiletype_BaseTile));
		ser.Serialize( tw , tileTypeInfo );

		tw.Close( );

		atlasTexture.GenerateAtlasToFile( );
		if ( forceFull ) {
			materialAssign.SimpleAssignFull( atlasTexture );
		}
		else {
			materialAssign.CorrectAssignMain( atlasTexture );
		}

		Debug.Log( "-------======--------" );
		Debug.Log( "Time to generate : " + ( EditorApplication.timeSinceStartup - timeToRender ) );
		abortGenerate = false;
		yield return null;
	}


	Texture2D GetSingleBatchTexture ( int batchSeed , BiomeSpawnerInformation centerNode , BiomeSpawnerInformation node0 , BiomeSpawnerInformation node1 , BiomeSpawnerInformation node2 , BiomeSpawnerInformation node3 , BiomeSpawnerInformation node4 , BiomeSpawnerInformation node5 )
	{

		CleanChildrenObjects( );
		int oldSeed = Random.seed;
		Random.InitState( batchSeed );
		Transform centerHexagon = GenerateBasicHexagonShape(centerNode);
		List<Transform> neighborHexagon = CreateNeighborHexagonShapes(node0, node1, node2, node3, node4, node5); //Creates neighboring hexagons in order of clockwise fashion.

		//--POPULATE HEXAGON HERE
		RunBatchSpawner( spawnedObjectsParent.transform , centerHexagon );

		//--TEXTURE HERE
		//Generate the pure alpha shape.  This is with only the central hexagon.
		foreach ( Transform neighbor in neighborHexagon ) { neighbor.gameObject.SetActive( false ); }

		//Generate the partial alpha and final image with just centra.
		Texture2D hexagonRender_AA = colorRenderCamera.GetComponent<CameraExportFromRenderTexture>().GetTexture(tileResolution);

		foreach ( Transform neighbor in neighborHexagon ) { neighbor.gameObject.SetActive( true ); }
		//Disable the children on hte bottom 3 hexagons.
		SetActiveChildren( neighborHexagon[2] , false );
		SetActiveChildren( neighborHexagon[3] , false );
		SetActiveChildren( neighborHexagon[4] , false );
		SetActiveChildren( neighborHexagon[1] , false );
		SetActiveChildren( neighborHexagon[0] , false );
		SetActiveChildren( neighborHexagon[5] , false );
		Texture2D hexagonRender_TopNeighborsAA = colorRenderCamera.GetComponent<CameraExportFromRenderTexture>().GetTexture(tileResolution);


		hexagonRender_AA = CameraExportFromRenderTexture.FrontColorToBackgroundAlpha( hexagonRender_AA , hexagonRender_TopNeighborsAA );
		hexagonRender_AA = CameraExportFromRenderTexture.AddTransparencyToSides( hexagonRender_AA );

		if ( destroyAfterComplete ) {
			CleanChildrenObjects( );
		}
		Random.InitState( oldSeed );
		return hexagonRender_AA;// colorRenderCamera.GetComponent<CameraExportFromRenderTexture>( ).GetTexture(  );
	}

	void CreateSingleBatch ( string fileName , int batchSeed , BiomeSpawnerInformation centerNode , BiomeSpawnerInformation node0 , BiomeSpawnerInformation node1 , BiomeSpawnerInformation node2 , BiomeSpawnerInformation node3 , BiomeSpawnerInformation node4 , BiomeSpawnerInformation node5 )
	{

		CleanChildrenObjects( );
		int oldSeed = Random.seed;
		Random.InitState( batchSeed );
		Transform centerHexagon = GenerateBasicHexagonShape(centerNode);
		List<Transform> neighborHexagon = CreateNeighborHexagonShapes(node0, node1, node2, node3, node4, node5); //Creates neighboring hexagons in order of clockwise fashion.
																												 //--POPULATE HEXAGON HERE
		RunBatchSpawner( spawnedObjectsParent.transform , centerHexagon );
		foreach ( Transform neighbor in neighborHexagon ) { RunBatchSpawner( neighbor , centerHexagon ); }

		//Push center up slightly.
		//--TEXTURE HERE
		//Generate the pure alpha shape.  This is with only the central hexagon.
		foreach ( Transform neighbor in neighborHexagon ) { neighbor.gameObject.SetActive( false ); }

		//Generate the partial alpha and final image with just centra.
		Texture2D hexagonRender_AA = colorRenderCamera.GetComponent<CameraExportFromRenderTexture>().GetTexture(tileResolution);

		foreach ( Transform neighbor in neighborHexagon ) { neighbor.gameObject.SetActive( true ); }
		//Disable the children on hte bottom 3 hexagons.
		SetActiveChildren( neighborHexagon[2] , false );
		SetActiveChildren( neighborHexagon[3] , false );
		SetActiveChildren( neighborHexagon[4] , false );
		SetActiveChildren( neighborHexagon[1] , false );
		SetActiveChildren( neighborHexagon[0] , false );
		SetActiveChildren( neighborHexagon[5] , false );
		Texture2D hexagonRender_TopNeighborsAA = colorRenderCamera.GetComponent<CameraExportFromRenderTexture>().GetTexture(tileResolution);

		hexagonRender_AA = CameraExportFromRenderTexture.FrontColorToBackgroundAlpha( hexagonRender_AA , hexagonRender_TopNeighborsAA );

		if ( CameraExportFromRenderTexture.CheckIfSidesAreTransparent( hexagonRender_AA ) == false && failCounter < failMax ) {
			failCounter++;
			Debug.Log( "Fail on : " + fileName );
			CreateSingleBatch( fileName , batchSeed , centerNode , node0 , node1 , node2 , node3 , node4 , node5 );
			Random.InitState( oldSeed );
			return;
		}
		if ( failCounter == failMax ) { Debug.LogError( "ERROR : REACHED MAX FORMAT ON : " + fileName ); }
		hexagonRender_AA = CameraExportFromRenderTexture.AddTransparencyToSides( hexagonRender_AA );

		colorRenderCamera.GetComponent<CameraExportFromRenderTexture>( ).ExportToPng( hexagonRender_AA , fileName + ".png" );

		if ( destroyAfterComplete ) {
			CleanChildrenObjects( );
		}
		Random.InitState( oldSeed );
	}
	void SetActiveChildren ( Transform tran , bool activeState )
	{
		Transform[] children = tran.GetComponentsInChildren<Transform>();
		foreach ( Transform child in children ) {
			if ( child != tran ) { child.gameObject.SetActive( activeState ); }
		}
	}

	//--Finds all bathc spawners, and chooses the lowest order to spawn objects with.  
	//----This generates all sub objects.
	void RunBatchSpawner ( Transform tran , Transform centerHexagon )
	{
		BatchSpawner currentSpawner = FindNextBatchSpawner(tran);
		while ( currentSpawner != null ) {
			currentSpawner.SpawnObjects( centerHexagon , spawnedObjectsParent.transform , hexagonMask , regularMask , noSpawnMask );
			currentSpawner = FindNextBatchSpawner( tran );
		}
	}

	//--Returns the batch spawner with lowest value.  
	BatchSpawner FindNextBatchSpawner ( Transform tran )
	{
		BatchSpawner[] batchSpawner = tran.GetComponentsInChildren<BatchSpawner>();
		if ( batchSpawner == null || batchSpawner.Length == 0 ) { return null; }
		BatchSpawner returnSpawner = batchSpawner[0];
		foreach ( BatchSpawner bs in batchSpawner ) {
			if ( bs.spawnOrder < returnSpawner.spawnOrder ) { returnSpawner = bs; }
		}
		return returnSpawner;
	}


	List<Transform> CreateNeighborHexagonShapes ( BiomeSpawnerInformation node0 , BiomeSpawnerInformation node1 , BiomeSpawnerInformation node2 , BiomeSpawnerInformation node3 , BiomeSpawnerInformation node4 , BiomeSpawnerInformation node5 )
	{
		//We should apply information to generate the correct hexagon
		List<Transform> returnList = new List<Transform>(6);
		Transform newAdd;

		newAdd = GenerateBasicHexagonShape( node0 );
		newAdd.name = " Base hexagon [0]";
		newAdd.localPosition = new Vector3( 0.0f , 0.0f , 1.0f );
		returnList.Add( newAdd );

		newAdd = GenerateBasicHexagonShape( node1 );
		newAdd.name = " Base hexagon [5]";

		newAdd.localPosition = new Vector3( 1.0f , 0.0f , 0.5f );
		returnList.Add( newAdd );

		newAdd = GenerateBasicHexagonShape( node2 );
		newAdd.name = " Base hexagon [4]";

		newAdd.localPosition = new Vector3( 1.0f , 0.0f , -0.5f );
		returnList.Add( newAdd );

		newAdd = GenerateBasicHexagonShape( node3 );
		newAdd.name = " Base hexagon [3]";

		newAdd.localPosition = new Vector3( 0.0f , 0.0f , -1.0f );
		returnList.Add( newAdd );

		newAdd = GenerateBasicHexagonShape( node4 );
		newAdd.name = " Base hexagon [2]";

		newAdd.localPosition = new Vector3( -1.0f , 0.0f , -0.5f );
		returnList.Add( newAdd );

		newAdd = GenerateBasicHexagonShape( node5 );
		newAdd.name = " Base hexagon [1]";

		newAdd.localPosition = new Vector3( -1.0f , 0.0f , 0.5f );
		returnList.Add( newAdd );

		return returnList;
	}

	//--In future, we can apply biome-specific information for this.
	Transform GenerateBasicHexagonShape ( BiomeSpawnerInformation spawnerInfo )
	{
		Transform returnhexagon = Instantiate(baseShapePrefab , spawnedObjectsParent.transform);
		returnhexagon.name = "Hexagon Shape Spawned";
		
		returnhexagon.gameObject.SetActive( true );
		returnhexagon.localPosition = new Vector3( 0 , 0 , 0 );
		Transform[] children = returnhexagon.GetComponentsInChildren<Transform>();
		Transform newSpawner;
		for ( int i = 0 ; i < children.Length ; ++i ) {
			if ( children[i] != returnhexagon ) {

				if ( children[i].GetComponent<SpawnerTag_Center>( ) != null ) {
					if ( spawnerInfo.centerNode != null ) {

						foreach ( Transform newNode in spawnerInfo.centerNode ) {
							if ( newNode != null ) {
								newSpawner = Instantiate( newNode , returnhexagon );
								newSpawner.localPosition = children[i].localPosition;
								newSpawner.SetParent( returnhexagon );
							}
						}

					}
				}

				if ( children[i].GetComponent<SpawnerTag_Grid>( ) != null ) {
					if ( spawnerInfo.gridNode != null ) {
						foreach ( Transform newNode in spawnerInfo.gridNode ) {
							if ( newNode != null ) {
								newSpawner = Instantiate( newNode , returnhexagon );
								newSpawner.localPosition = children[i].localPosition;
								newSpawner.SetParent( returnhexagon );
							}

						}
					}
				}

				if ( children[i].GetComponent<SpawnerTag_InnerEdgeCenter>( ) != null ) {
					if ( spawnerInfo.innerRingEdgeCenterNode != null ) {
						foreach ( Transform newNode in spawnerInfo.innerRingEdgeCenterNode ) {
							if ( newNode != null ) {
								newSpawner = Instantiate( newNode , returnhexagon );
								newSpawner.localPosition = children[i].localPosition;
								newSpawner.SetParent( returnhexagon );
							}
						}

					}
				}

				if ( children[i].GetComponent<SpawnerTag_InnerVertexCenter>( ) != null ) {
					if ( spawnerInfo.innerRingVertexCenterNode != null ) {
						foreach ( Transform newNode in spawnerInfo.innerRingVertexCenterNode ) {
							if ( newNode != null ) {
								newSpawner = Instantiate( newNode , returnhexagon );
								newSpawner.localPosition = children[i].localPosition;
								newSpawner.SetParent( returnhexagon );
							}
						}
					}
				}

				if ( children[i].GetComponent<Spawnertag_OuterEdgeCenter>( ) != null ) {
					if ( spawnerInfo.outerRingEdgeCenterNode != null ) {
						foreach ( Transform newNode in spawnerInfo.outerRingEdgeCenterNode ) {
							if ( newNode != null ) {
								newSpawner = Instantiate( newNode , returnhexagon );
								newSpawner.localPosition = children[i].localPosition;
								newSpawner.SetParent( returnhexagon );
							}
						}
					}
				}

				if ( children[i].GetComponent<SpawnerTag_OuterVertexCenter>( ) != null ) {
					if ( spawnerInfo.outerRingVertexCenterNode != null ) {
						foreach ( Transform newNode in spawnerInfo.outerRingVertexCenterNode ) {
							if ( newNode != null ) {
								newSpawner = Instantiate( newNode , returnhexagon );
								newSpawner.localPosition = children[i].localPosition;
								newSpawner.SetParent( returnhexagon );
							}
						}
					}
				}
				DestroyImmediate( children[i].gameObject );
			}
		}
		return returnhexagon;
	}


	void CleanChildrenObjects ()
	{
		if ( spawnedObjectsParent != null ) { DestroyImmediate( spawnedObjectsParent ); }
		Transform[] childrenList = GetComponentsInChildren<Transform>();
		for ( int i = 0 ; i < childrenList.Length ; ++i ) {
			if ( childrenList[i] != null && childrenList[i] != transform ) {
				DestroyImmediate( childrenList[i].gameObject );
			}
		}
		spawnedObjectsParent = new GameObject( "Spawned Objects Parent" );
		spawnedObjectsParent.transform.parent = transform;

	}
}
[System.Serializable]
public class BiomeBase {

	//--XML
	public string biomeName = "Unset";
	public int materialType = 0;
	public Color biomeInputColor = Color.white;
	public Color hexagonVertexColor = Color.white;
	public float hexagonVertexColorNoise = 1.0f;
	public int orderHeightIndex = 1;
	public float tileMeshScale = 1.0f;

	public BiomeSpawnerInformation basicTile;
	public BiomeSpawnerInformation centerSpecialTile;
	public bool useSpecialCenterTile = false; //This spawns in the special tile as the center tile when surrounded
	public bool useNoNeighborsWithSpecial = false; //If surrounded by enabled tiles, using special, we disable the outside tiles.  

	public bool useSpecialEmptyTile = false;
	public BiomeSpawnerInformation emptySpecialTile;

	public bool useSpecialNoSpawnSides = false;

}


//This is the initial spawner that goes on nodes.  If a piece isn't used, it won't spawn on it.
[System.Serializable]
public class BiomeSpawnerInformation {
	public Transform[] centerNode;
	public Transform[] gridNode;
	public Transform[] innerRingEdgeCenterNode;
	public Transform[] innerRingVertexCenterNode;
	public Transform[] outerRingEdgeCenterNode;
	public Transform[] outerRingVertexCenterNode;

}
