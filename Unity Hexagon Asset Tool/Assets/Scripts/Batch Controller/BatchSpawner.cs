﻿using System.Collections.Generic;
using UnityEngine;


//--The object to spawn.  May be a spawner or something else.
[System.Serializable]
public class SpawnObject {
	public string name = "unset";

	[Tooltip("Types of variations for this object")]
	public List<Transform> spawnObject; //Object to spawn
										//--
	[Tooltip("Can this object spawn at all")]
	public bool spawnAble = true;
	[Tooltip("Can this only spawn on the center hexagon, and not neighbor hexagons")]
	public bool onlySpawnOnCentral = false; //Only allowed to spawn on central object. 




	[Tooltip("Force anything spawned to be set to 0 height.")]
	public bool forceHeightToZero = false;
	[Tooltip("Adds additional height to the spawn.  Added after force height to 0.")]
	public float spawnHeightOffset = 0.0f; //Height added

	[Tooltip("If this checks distance to other spawned objects that exist already")]
	public bool ignoreOtherObjects = false; //Used to ignore distnace to other objects.
	public float minimumDistanceFromObjects = 0.1f; //Min distance away from other objects
													//	[Tooltip("Maximum distance away from center of current hexagon this is allowed to be")]
													//public float maximumDistanceFromHexagonCenter = 10.0f; //Max distance from whatever hexagon it spawns on

	[Tooltip("Adds a constant forward rotation before additional rotations take place.")]
	public float basicForwardRotation = 55.0f; //How much slant it initally has

	[Tooltip("Slight rotation in X,Y, Z from 0 to 180")]
	public Vector3 randomRotationRange = new Vector3(2.5f, 0.0f, 180.0f);//Forward tilt, sideways tilt, top down rotation

	public bool rotationLookAtCenter = false;

	public bool randomScale_XYCombined = false; //XY scale equally.
	public bool randomScale_XZCombined = false; //XZ scale equally.  these 2 settings means it scales completely.
	public Vector3 randomScaleRange = new Vector3(0.0f, 0.0f, 0.0f); //Sideways scale, forward/back scale, vertical scale

	[Tooltip("Random offset max this is allowed to offset")]
	public float maximumOffsetFromSpawner = 1.0f;
	[Tooltip("Value that determines how packed this is from the center.  0 means everything is packed, 1 is linear.  ")]
//	public AnimationCurve offsetDistribution = new AnimationCurve();
	//public float maximumOffset_Distribution = 0.2f; //Chance that range is increased by 1.3x for single spawn.  Little bit of variation.

	public bool ignoreRaycast = false; //Used for floor pieces.  Does not use raycasting.  Height stays at slightly above0.

	//--Spawn limits
	[Tooltip("Maximum spawns allowed from this specific spawner.  LIMITED BY TRIES")]
	public int maxSpawnsFromPoint = 1000; //Limit min distance from objects
	[Tooltip("Maximum attempts at spawning.  A successful spawn is an attempt.")]
	public int maxTryAttempts = 500; //How many times is this allowed to fail
									 //[Tooltip("If true this is forced to parent to the center hexagon even if spawned on a neighbor")]
									 //UNUSED - Pieces parent to what hexagon they land on.  so this kind of happens automatically
									 //public bool alwaysParentToCenter = false; //If true, it forces itself to be parented to center.  
	[Tooltip("If using parent to center, forces a distance check.  ")]
	public float maxDistToHexagonCenter = 1.0f; //If setting to parent, this fails if it is too far away.
	int spawnCount = 0;
	int tryCounter = 0;
	[Tooltip("Priority of this to spawn.  Higher spawnorder is higher priority")]
	public int spawnOrder = 0;
	public bool debugInfo = false;

	public void ResetSpawner ()
	{
		SpawnCount = 0;
		TryCounter = 0;

	}
	public int SpawnCount {
		get {
			return spawnCount;
		}

		set {
			spawnCount = value;
		}
	}


	public int TryCounter {
		get {
			return tryCounter;
		}

		set {
			tryCounter = value;
		}
	}

	//Returns a distnace to offset from zero.  This prefers being close to 0.
	public Vector3 GetOffset ()
	{
		float offsetValue = Random.value;
		//float distOffset = offsetDistribution.Evaluate(offsetValue);
		//distOffset = Mathf.Clamp( distOffset , 0.0f , 1.0f );
		//offsetValue *= distOffset;
		float distAngle = Random.value*360.0f * Mathf.Deg2Rad;

		offsetValue = Random.value;
		return new Vector3( offsetValue * maximumOffsetFromSpawner * Mathf.Cos( distAngle ) , 0 , offsetValue * maximumOffsetFromSpawner * Mathf.Sin( distAngle ) );
	}
	//public Vector3 GetOffsetVector ()
	//{
	//	Vector2 offsetVector = GetOffset();
	//	float xVal = offsetVector.x;
	//	float zVal = offsetVector.y;
	//	if (xVal == 0 && zVal == 0 ) { return new Vector3( 0 , 0 , 0 ); }
	//	float normalizeMult = (maximumOffsetFromSpawner / Mathf.Sqrt(Mathf.Pow(xVal, 2.0f) + Mathf.Pow(zVal, 2.0f))  )* (1.0f - Mathf.Sqrt( Random.value ));

	//	return new Vector3( xVal * normalizeMult , 0 , zVal * normalizeMult );
	//}
	public bool CanSpawn ()
	{
		if ( !spawnAble ) {
			return false;
		}
		if ( SpawnCount >= maxSpawnsFromPoint ) {
			if ( debugInfo ) { Debug.Log( "Spawn count for : " + name + "  reached" ); }
			spawnAble = false;

			return false;
		}
		if ( TryCounter >= maxTryAttempts ) {
			if ( debugInfo ) { Debug.Log( "Try count for : " + name + "  reached" ); }
			spawnAble = false;

			return false;
		}
		return true;
	}
}
[System.Serializable]
public class BatchSpawner : MonoBehaviour {
	//List of game objects.  
	//public int maxTryCount = 500;
	[Tooltip("Chance for this spawner to exist")]
	public float spawnChance = 1.0f;

	[Tooltip("If this spawner is only allowed on neighbors and not center hexagon")]
	public bool spawnerOnlyOnNeighbor = false ;// If true, deletes spawners on center node BEFORE they are moved

	public float spawnerOffset = 0.0f;


	bool noMoreSpawns = false;
	[Tooltip("Spawn priority of this spawner.  Higher is higher priority")]
	public int spawnOrder = 0;
	[Tooltip("Objects this spawner may spawn")]
	public List<SpawnObject> spawnList;

	List<Transform> SearchList = new List<Transform>();
	int spawnOrderIncrementer = 0; //Continually searches upward in the list.  Iterates through the entire list to find highest value.

	public Object onSpawnScript ;

	public void SpawnObjects ( Transform centralHexagon , Transform parentObject , LayerMask hexagonLayerMask , LayerMask rayLayerMask , LayerMask noSpawnMask )
	{

		SpawnObject spawnObject;
		//Destroy if unlucky
		if ( spawnChance < Random.value ) { DestroyImmediate( gameObject ); return; }

		//Destroy if it needed to spawn on a neigh
		if ( centralHexagon == transform.parent && spawnerOnlyOnNeighbor ) { DestroyImmediate( gameObject ); return; }


		float offsetAngle = Random.value * Mathf.PI * 2.0f;
		//Debug.Log( "Old position :" + transform.position );
		transform.localPosition += new Vector3( Random.value * spawnerOffset * Mathf.Cos( offsetAngle ) , Random.value * spawnerOffset * Mathf.Sin( offsetAngle ) , 0 );

		OnObjectSpawnerSpawn( );

		while ( noMoreSpawns == false ) {
			//Choose which random object we are trying.
			spawnObject = GetValidSpawnObject( );
			if ( AttemptToCreateObject( spawnObject , parentObject , hexagonLayerMask , rayLayerMask , noSpawnMask , centralHexagon ) ) {
				//	i = 0;
				OnObjectSpawnSuccess( );
				spawnObject.SpawnCount++;
			}
			else { spawnObject.TryCounter++; OnObjectSpawnFailure( ); }

		}
		//Destroy this at the end, as we no longer need this.
		DestroyImmediate( gameObject );

	}
	SpawnObject GetValidSpawnObject ()
	{
		List<SpawnObject> spawnTierList = new List<SpawnObject>();
		//spawnOrderIncrementer
		int maxSpawnOrderTier = 0;
		foreach ( SpawnObject spawnObject in spawnList ) {
			if ( spawnObject.CanSpawn( ) ) {
				maxSpawnOrderTier = spawnObject.spawnOrder;
			}
		}

		spawnTierList = GetSpawnObjectsAtTier( maxSpawnOrderTier );
		if ( spawnTierList.Count == 0 ) { noMoreSpawns = true; return spawnList[0]; }
		return spawnTierList[Random.Range( 0 , spawnTierList.Count - 1 )];
	}



	List<SpawnObject> GetSpawnObjectsAtTier ( int maxLevel )
	{
		List<SpawnObject> returnList = new List<SpawnObject>();
		foreach ( SpawnObject spawnObject in spawnList ) {
			if ( spawnObject.spawnOrder == spawnOrderIncrementer && spawnObject.CanSpawn( ) ) { returnList.Add( spawnObject ); }
		}

		if ( returnList.Count == 0 && spawnOrderIncrementer < maxLevel ) {
			//	if (tierLevel == maxLevel ) { }
			spawnOrderIncrementer += 1;
			//	Debug.Log( "INCREMENT TO : " + spawnOrderIncrementer );
			return GetSpawnObjectsAtTier( maxLevel );
		}


		return returnList;
	}

	void OnObjectSpawn ( GameObject newObject )
	{
		newObject.BroadcastMessage( "SpawnSuccess" , null , SendMessageOptions.DontRequireReceiver );
		//GameObject[] children = newObject.GetComponentsInChildren<GameObject>();
		//foreach (GameObject child in children ) {
		//	child.SendMessage( "SpawnSuccess" );
		//}

	}
	bool AttemptToCreateObject ( SpawnObject spawnObject , Transform parentObject , LayerMask hexagonLayerMask , LayerMask rayLayerMask , LayerMask noSpawnMask , Transform centralHexagon )
	{
		//Createa  new offset to try.
		Transform baseHexagon;

		Vector3 spawnPoint = transform.position + new Vector3( 0 , 100.0f , 0) + spawnObject.GetOffset();
		//float initialRayHeight = 100.0f;
		//Raycast down.  
		//-Raycast to hexagons.  If this doesn't hit it, then this returns false.  We do not support added terrain off the hexagon.

		if ( spawnObject.CanSpawn( ) == false ) { return false; }

		RaycastHit rayHit = RaycastHexagon( spawnPoint , hexagonLayerMask );
		RaycastHit rayNoSpawnhit;///= RaycastHexagon( spawnPoint , noSpawnMask );
		if ( rayHit.transform ) {
			if ( spawnObject.onlySpawnOnCentral && rayHit.transform != centralHexagon ) {
				if ( spawnObject.debugInfo == true ) {
					Debug.Log( "Failed spawn" + spawnObject.name + " : Can only spawn on central.  Landed on " + rayHit.transform.name );
				}
				return false;
			}
			//if (spawnObject.onlySpawnNotOnCentral && rayHit.transform == centralHexagon ) {
			//		return false;
			//	}
			baseHexagon = rayHit.transform;
			spawnPoint.y = rayHit.point.y;

			//	if ( Vector3.Distance( spawnPoint , baseHexagon.position ) >= spawnObject.maximumDistanceFromHexagonCenter ) { return false; }

			//We hit something. 
		}
		//If this point did not hit a hexagon, fail.
		else {
			if ( spawnObject.debugInfo == true ) {
				Debug.Log( "Failed spawn" + spawnObject.name + " : failed to raycast a hexagon" );
			}

			return false;
		}
		//We have a parent hexagon to spawn objects on.

		if ( spawnObject.ignoreRaycast == false ) {
			//	spawnPoint.y = 100.0f;
			//	rayHit = RaycastHexagon( spawnPoint , noSpawnMask );
			//	if ( rayHit.transform && rayHit.point.y >= spawnPoint.y) {
			//		return false;
			//	}
			spawnPoint.y = 100.0f;
			rayHit = RaycastHexagon( spawnPoint , rayLayerMask );
			if ( rayHit.transform == null ) {
				if ( spawnObject.debugInfo == true ) {
					Debug.Log( "Failed spawn " + spawnObject.name + " : failed to raycast a hexagon" );
				}
				return false;
			}

			rayNoSpawnhit = RaycastHexagon( spawnPoint , noSpawnMask );
			if (rayNoSpawnhit.transform != null ) {
			//	Debug.Log( "Raycast hit : " + rayNoSpawnhit.transform.name + "   spawnobject : "+ spawnObject.name );
			}
			if ( rayNoSpawnhit.transform != null && rayNoSpawnhit.point.y >= rayHit.point.y ) {
				if ( spawnObject.debugInfo == true ) {
					Debug.Log( "Failed spawn " + spawnObject.name + " : failed due to raycast hitting something above bsae hexagon layer" );
				}
				return false;
			}


			//if (rayHit.transform.gameObject.layer == )
			//--If this is too close to everything, then we fail.
			spawnPoint = rayHit.point;
			spawnPoint.y += spawnObject.spawnHeightOffset + 0.02f * Random.value; ;
			//	if (spawnPoint.y < 0.0f ) { return false; }
			if ( spawnObject.forceHeightToZero ) { spawnPoint.y = spawnObject.spawnHeightOffset + 0.02f * Random.value; ; }

			if ( !spawnObject.ignoreOtherObjects && !NoObjectCloseToPoint( spawnObject.minimumDistanceFromObjects , spawnPoint , parentObject ) ) {
				if ( spawnObject.debugInfo == true ) {
					Debug.Log( "Failed spawn " + spawnObject.name + " : Failed due to other objects being nearby" );
				}
				return false;
			}
		}
		else { spawnPoint.y = spawnObject.spawnHeightOffset + 0.02f * Random.value; }

		//	if (Vector3.Distance(spawnPoint, baseHexagon.position) >= spawnObject.maximumDistanceFromHexagonCenter ) { return false; }
		///	if ( spawnObject.alwaysParentToCenter ) {
		///	

		//	if (baseHexagon != centralHexagon && spawnObject.canSpawnOnNeighbors == false ) { return false; }
		//	if ( Vector3.Distance( centralHexagon.transform.position , spawnPoint ) > spawnObject.alwaysParentToCenterDist ) {
		if ( HexagonDistance( spawnPoint - baseHexagon.position ) > spawnObject.maxDistToHexagonCenter ) {
			//	if ( Vector3.Distance( centralHexagon.transform.position , spawnPoint ) > spawnObject.alwaysParentToCenterDist ) {
			if ( spawnObject.debugInfo == true ) {
				Debug.Log( "Failed spawn " + spawnObject.name + " : Failed spawn : too far to hexagon spawn center." );
			}
			return false;

		}

		//	}

		//--
		Transform newObject = Instantiate(spawnObject.spawnObject[Random.Range(0, spawnObject.spawnObject.Count)], baseHexagon);
		newObject.position = spawnPoint;



		float xScaleMult =  Random.Range(-spawnObject.randomScaleRange.x , spawnObject.randomScaleRange.x);
		//randomScale_XYCombined
		Vector3 newScaleMult = new Vector3( 1.0f + xScaleMult ,
										1.0f + Random.Range( -spawnObject.randomScaleRange.y , spawnObject.randomScaleRange.y ) ,
										1.0f + Random.Range( -spawnObject.randomScaleRange.z , spawnObject.randomScaleRange.z ) );
		if ( spawnObject.randomScale_XYCombined ) {
			newScaleMult.y = 1.0f + xScaleMult;
		}
		if ( spawnObject.randomScale_XZCombined ) {
			newScaleMult.z = 1.0f + xScaleMult;
		}
		newObject.localScale = new Vector3( newObject.localScale.x * newScaleMult.x , newObject.localScale.y * newScaleMult.y , newObject.localScale.z * newScaleMult.z );
		newObject.gameObject.SetActive( true );

		newObject.localPosition = new Vector3( newObject.localPosition.x , newObject.localPosition.y + Random.Range( -0.04f , 0.04f ) , newObject.localPosition.z );

		Vector3 lookAtPosition = new Vector3(0,0,0);
		if ( spawnObject.rotationLookAtCenter == true ) {
			lookAtPosition = newObject.parent.position;
			lookAtPosition.y = newObject.position.y;
			newObject.transform.LookAt( lookAtPosition , Vector3.up );
			newObject.Rotate( new Vector3( 90f , 0f , -180f ) , Space.Self );
		}

		newObject.Rotate( new Vector3( Random.Range( -spawnObject.randomRotationRange.x , spawnObject.randomRotationRange.x ) + spawnObject.basicForwardRotation , 0.0f , 0.0f ) );
		newObject.Rotate( new Vector3( 0.0f , Random.Range( -spawnObject.randomRotationRange.y , spawnObject.randomRotationRange.y ) , 0.0f ) );
		newObject.Rotate( new Vector3( 0.0f , 0.0f , Random.Range( -spawnObject.randomRotationRange.z , spawnObject.randomRotationRange.z ) ) );


		//spawnObject.SpawnCount++;
		SearchList.Add( newObject );
		if ( spawnObject.debugInfo ) { Debug.Log( "Spawned object : " + spawnObject.name ); }
		if ( spawnObject.debugInfo == true ) {
			Debug.Log( "SUCCESS spawn " + spawnObject.name + " : was successful" );
		}
		//	if ( newObject.GetComponent<BatchSpawner>( ) != null ) { newObject.GetComponent<BatchSpawner>( ).SpawnObjects( centralHexagon , parentObject , hexagonLayerMask , rayLayerMask , noSpawnMask ); }

		OnObjectSpawn( newObject.gameObject );
		return true;
	}
	public float HexagonDistance ( Vector3 checkPoint )
	{
		checkPoint.x = Mathf.Abs( checkPoint.x );
		checkPoint.z = Mathf.Abs( checkPoint.z );
		//This is now looking at only 1 quadrant.
		//If above the cutoff point in the hexagon (top limit is 0.5, top right vertice is < 0.24, 0.5 > , right vertice is <0.75, 0>
		if ( checkPoint.z >= ( 0.5f / 0.24f ) * checkPoint.x ) {
			return checkPoint.z / 0.5f;

		}
		//If not in top region, it in the right diagnal region
		else {
			//return 50.0f;

			return ( ( checkPoint.x + checkPoint.z ) / 0.75f );
		}

		return 0;
	}

	public RaycastHit RaycastHexagon ( Vector3 worldPosition , LayerMask layerMask )
	{
		RaycastHit rayHit = new RaycastHit();

		if ( Physics.Raycast( worldPosition , Vector3.down , out rayHit , Mathf.Infinity , layerMask ) ) {
			return rayHit;
		}

		return rayHit;
	}

	//Grabs all children belonging to parent and checks if any are closer then check distance.  the componet NoSpawnBlock enables us to skip over objects.  This is the final spawned objects position, which may include Y heihgt.
	public bool NoObjectCloseToPoint ( float checkDistance , Vector3 worldPosition , Transform parentObject )
	{
		//	Transform[] children = parentObject.GetComponentsInChildren<Transform>();
		foreach ( Transform child in SearchList ) {
			if ( child != null ) {
				if ( Vector3.Distance( child.position , worldPosition ) <= checkDistance && child.GetComponent<NoSpawnBlock>( ) == null ) { return false; }
			}
		}

		return true;
	}
	//When this starts

	System.Type methodType;
	System.Reflection.MethodInfo methodInfoSpawn ;
	System.Reflection.MethodInfo methodInfoSpawnSuccess ;
	System.Reflection.MethodInfo methodInfoSpawnFailure ;
	void OnObjectSpawnerSpawn ()
	{
		//--Initialize values 
		if ( onSpawnScript == null ) { return; }

		if ( onSpawnScript.GetType( ) != null ) {
			methodType = onSpawnScript.GetType( );
		}
		if ( methodType != null && methodType.GetMethod( "OnObjectSpawn" ) != null ) {
			methodInfoSpawn = methodType.GetMethod( "OnObjectSpawn" );
		}
		if ( methodType != null && methodType.GetMethod( "OnObjectSpawnSuccess" ) != null ) {
			methodInfoSpawnSuccess = methodType.GetMethod( "OnObjectSpawnSuccess" );
		}

		if ( methodType != null && methodType.GetMethod( "OnObjectSpawnFailure" ) != null ) {
			methodInfoSpawnFailure = methodType.GetMethod( "OnObjectSpawnFailure" );
		}


		Object[] objectList = null;




		if ( methodInfoSpawn != null ) {
			objectList = new Object[1];
			objectList[0] = ( Object )transform.parent;
			methodInfoSpawn.Invoke( onSpawnScript , objectList );
			return;
		}

	}
	void OnObjectSpawnSuccess ()
	{
		if ( onSpawnScript == null || methodInfoSpawnSuccess == null ) { return; }
		//if ( onSpawnScript.GetType().GetMethod("OnObjectSpawnSuccess" ) != null ) {
		methodInfoSpawnSuccess.Invoke( onSpawnScript , null );
		//}
	}

	void OnObjectSpawnFailure ()
	{
		if ( onSpawnScript == null || methodInfoSpawnFailure == null ) { return; }
		//if ( onSpawnScript.GetType( ).GetMethod( "OnObjectSpawnFailure" ) != null ) {
		methodInfoSpawnFailure.Invoke( onSpawnScript , null );
		//	}
	}
}

