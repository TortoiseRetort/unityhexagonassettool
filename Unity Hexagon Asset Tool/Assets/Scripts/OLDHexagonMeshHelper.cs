﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class OLDHexagonMeshHelper : MonoBehaviour {
//	using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class HexagonMeshCreationHelper {
//	const int hexagonGridSize = 32; //Number of points in a hexagon
//									//const int hexagonTextureGrids = 4; //How many times the texture grid is broken up.  used for texture offsets.  
//									//--Contains mesh data for an individual hexagon.
//	Vector3[] simpleVertices;
//	Vector3[] simpleNormals;
//	//public Vector2[] simpleUV;
//	//public Color32[] simpleColors; 
//	int[] simpleTriangles;

//	public Vector2[]simpleUV;
//	//--Unity mesh class. Will hold all the important things like the full mesh, UV data, ect.
//	Mesh hexagonMeshBase;
//	Vector3[] meshVertices;
//	int[] meshTriangles;
//	Vector2[] meshUV ;
//	Color32[] meshColors;





//	//Easily accessable information related to the hexagon size.
//	public float totalWidth = 0.0f;
//	public float totalHeight = 0.0f;
//	public void CreateHexagonMeshObject ( Transform transformObject )
//	{
//		GenerateHexagonSimple( );
//		hexagonMeshBase = new Mesh( );

//		meshVertices = new Vector3[6 * hexagonGridSize * hexagonGridSize];
//		meshTriangles = new int[12 * hexagonGridSize * hexagonGridSize];
//		meshUV = new Vector2[6 * hexagonGridSize * hexagonGridSize];
//		meshColors = new Color32[6 * hexagonGridSize * hexagonGridSize];
//		for ( int i = 0 ; i < meshColors.Length ; ++i ) {
//			//	simpleUV[i] = simpleUV[i] * 1.0f / 4.0f;
//			meshColors[i].b = 255;
//			meshColors[i].r = 255;
//			meshColors[i].g = 255;
//			meshColors[i].a = 255;
//		}

//		for ( int x = 0 ; x < hexagonGridSize ; ++x ) {
//			for ( int y = 0 ; y < hexagonGridSize ; ++y ) {
//				AddHexagonAtPoint( x , y );
//			}
//		}


//		hexagonMeshBase.vertices = new Vector3[6 * hexagonGridSize * hexagonGridSize];
//		hexagonMeshBase.vertices = meshVertices;

//		hexagonMeshBase.triangles = new int[12 * hexagonGridSize * hexagonGridSize];
//		hexagonMeshBase.triangles = meshTriangles;

//		hexagonMeshBase.uv = new Vector2[6 * hexagonGridSize * hexagonGridSize];
//		hexagonMeshBase.uv = meshUV;

//		hexagonMeshBase.colors32 = meshColors;
//		hexagonMeshBase.RecalculateNormals( );
//		hexagonMeshBase.RecalculateBounds( );
//		transformObject.GetComponent<MeshFilter>( ).mesh = hexagonMeshBase;

//	}
//	void AddHexagonAtPoint ( int positionX , int positionY )
//	{
//		int offsetGridX = 2; //How many points we offset by.
//		int offsetGridY = 2; //How many points we go up by.

//		int helperOffset = 0;

//		//General idea here is we may need special adjustment from each node.
//		float perXPointDist = simpleVertices[2].x - simpleVertices[1].x ;
//		float perYPointDist = simpleVertices[3].z - simpleVertices[4].z ;

//		totalWidth = perXPointDist * hexagonGridSize * 2.0f;
//		totalHeight = perYPointDist * hexagonGridSize * 2.0f;
//		//We need to adjust our totla position too.  
//		if ( positionX % 2 == 1 ) { helperOffset = -1; }
//		for ( int i = 0 ; i < simpleVertices.Length ; ++i ) {
//			//Get the total block offset, plus our current working offset.
//			meshVertices[( ( ( positionX * hexagonGridSize ) + positionY ) * simpleVertices.Length ) + i] = simpleVertices[i] + new Vector3(
//																		offsetGridX * positionX * perXPointDist , 0.0f ,
//																		offsetGridY * positionY * perYPointDist + ( helperOffset * perYPointDist ) );// *perXPointDist , 0.0f,
//		}
//		for ( int i = 0 ; i < simpleTriangles.Length ; ++i ) {
//			meshTriangles[( ( ( positionX * hexagonGridSize ) + positionY ) * simpleTriangles.Length ) + i] = simpleTriangles[i] + ( ( ( positionX * hexagonGridSize ) + positionY ) * simpleVertices.Length );
//		}

//		for ( int i = 0 ; i < simpleUV.Length ; ++i ) {
//			meshUV[( ( ( positionX * hexagonGridSize ) + positionY ) * simpleUV.Length ) + i] = simpleUV[i] * ( 1.0f / 4.0f );//+ (((x*hexagonGridSize) + y) * simpleTriangles.Length);
//		}
//	}

//	//--Generates the simple mesh informatoin to create a single hexagon.
//	void GenerateHexagonSimple ()
//	{
//		int offsetX = 0;
//		int offsetY = 0;
//		float scaleX = 1.0f/2.0f;
//		float scaleY = (1.0f/2.0f);//1.5f; //Don't think this is needed.
//		simpleVertices = new Vector3[6];
//		simpleNormals = new Vector3[6];
//		simpleTriangles = new int[12];
//		simpleUV = new Vector2[6];
//		//Place vertice positions.
//		simpleVertices[0] = new Vector3( ( ( offsetX + 0.0f ) * scaleX ) , 0.0f , ( ( offsetY + 1.0f ) * scaleY ) );
//		simpleVertices[1] = new Vector3( ( ( offsetX + 1.0f ) * scaleX ) , 0.0f , ( ( offsetY + 2.0f ) * scaleY ) );
//		simpleVertices[2] = new Vector3( ( ( offsetX + 2.0f ) * scaleX ) , 0.0f , ( ( offsetY + 2.0f ) * scaleY ) );
//		simpleVertices[3] = new Vector3( ( ( offsetX + 3.0f ) * scaleX ) , 0.0f , ( ( offsetY + 1.0f ) * scaleY ) );
//		simpleVertices[4] = new Vector3( ( ( offsetX + 2.0f ) * scaleX ) , 0.0f , ( ( offsetY + 0.0f ) * scaleY ) );
//		simpleVertices[5] = new Vector3( ( ( offsetX + 1.0f ) * scaleX ) , 0.0f , ( ( offsetY + 0.0f ) * scaleY ) );

//		//Triangles
//		int baseTri = 0;
//		int vertA = 0;
//		int vertB = 0;
//		int vertC = 0;
//		vertA = 5; vertB = 0; vertC = 1;
//		simpleTriangles[baseTri + 0] = vertA; simpleTriangles[baseTri + 1] = vertB; simpleTriangles[baseTri + 2] = vertC; baseTri += 3;
//		simpleNormals[0] = VertexNormal( vertA , vertB , vertC );

//		vertA = 5; vertB = 1; vertC = 2;
//		simpleTriangles[baseTri + 0] = vertA; simpleTriangles[baseTri + 1] = vertB; simpleTriangles[baseTri + 2] = vertC; baseTri += 3;
//		simpleNormals[1] = VertexNormal( vertA , vertB , vertC );

//		vertA = 5; vertB = 2; vertC = 3;
//		simpleTriangles[baseTri + 0] = vertA; simpleTriangles[baseTri + 1] = vertB; simpleTriangles[baseTri + 2] = vertC; baseTri += 3;
//		simpleNormals[2] = VertexNormal( vertA , vertB , vertC );

//		vertA = 5; vertB = 3; vertC = 4;
//		simpleTriangles[baseTri + 0] = vertA; simpleTriangles[baseTri + 1] = vertB; simpleTriangles[baseTri + 2] = vertC; baseTri += 3;
//		simpleNormals[3] = VertexNormal( vertA , vertB , vertC );

//		for ( int i = 0 ; i < simpleUV.Length ; ++i ) {

//			simpleUV[i] = new Vector2( simpleVertices[i].x * ( 2.0f / 3.0f ) , simpleVertices[i].z );
//			if ( simpleUV[i].x >= 0.5f ) {
//				simpleUV[i].x -= 0.001f;
//			}
//			else { simpleUV[i].x += 0.001f; }
//			if ( simpleUV[i].y >= 0.5f ) {
//				simpleUV[i].y -= 0.001f;
//			}
//			else { simpleUV[i].y += 0.001f; }
//		}


//	}
//	Vector3 VertexNormal ( int vertA , int vertB , int vertC )
//	{
//		//b-a , c-a
//		return Vector3.Normalize( Vector3.Cross( simpleVertices[vertB] - simpleVertices[vertA] , simpleVertices[vertC] - simpleVertices[vertA] ) );
//	}
//}

