﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VariationCountCalculator : MonoBehaviour {
	public int minVariations = 2;
	public float variationPower = 1.5f;
	//Add/remove how many variations are to be allowed for each type.
	public int[] variationBase = new int[7];

	[SerializeField]
	public List<VariationCount> variationCounts = new List<VariationCount>(64);

	public int tileSize = 128;


	public int totalVariationCounter = 0;
	public int resolutionNeed = 0;
	public void Awake ()
	{
		SetupCounts( );
	}
	void SetupCounts ()
	{
		variationCounts = new List<VariationCount>( );

		VariationCount addingCount = new VariationCount();
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 7 );
		variationCounts.Add( addingCount );

		addingCount = new VariationCount( );
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 6 );
		addingCount.variationArray.Add( 1 );
		variationCounts.Add( addingCount );

		addingCount = new VariationCount( );
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 5 );
		addingCount.variationArray.Add( 2 );
		variationCounts.Add( addingCount );

		addingCount = new VariationCount( );
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 5 );
		addingCount.variationArray.Add( 1 );
		addingCount.variationArray.Add( 1 );
		variationCounts.Add( addingCount );

		addingCount = new VariationCount( );
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 4 );
		addingCount.variationArray.Add( 3 );
		variationCounts.Add( addingCount );

		addingCount = new VariationCount( );
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 4 );
		addingCount.variationArray.Add( 2 );
		addingCount.variationArray.Add( 1 );
		variationCounts.Add( addingCount );

		addingCount = new VariationCount( );
		addingCount.variationArray = new List<int>( );
		addingCount.variationArray.Add( 3 );
		addingCount.variationArray.Add( 2 );
		addingCount.variationArray.Add( 2 );
		variationCounts.Add( addingCount );
		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 4 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 3 );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 3 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 2 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

		//addingCount = new VariationCount( );
		//addingCount.variationArray = new List<int>( );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//addingCount.variationArray.Add( 1 );
		//variationCounts.Add( addingCount );

	}
	public void Update ()
	{
		SetupCounts( );
		int variationBaseValue = 0;
		totalVariationCounter = 0;
		foreach (VariationCount count in variationCounts ) {
			variationBaseValue = 0;
			foreach (int varArray in count.variationArray ) {
				variationBaseValue += variationBase[varArray - 1];
			}
			//This is how many variations we will need of each type.
			count.variationsPerType =  variationBaseValue;
			count.variationsPerType = Mathf.Max( minVariations , (int)Mathf.Pow(Mathf.Max(0, count.variationsPerType ) , variationPower) );
			//Find how many types we have.
			count.GetTypePossibilities( );
			count.variationsNeededTotal = count.variationsTypeTotal * count.variationsPerType;
	//		count.ReportLog( );
			totalVariationCounter += count.variationsNeededTotal;
		}
		resolutionNeed = (int) Mathf.Sqrt( totalVariationCounter ) * tileSize;
		//Debug.Log( "Updated" );
		
	}

}

[System.Serializable]
public class VariationCount {
	public List<int> variationArray = new List<int>(6); //Number of variations by type.


	public int variationsPerType = 0; //Basic value of how many variations per type.
	public int variationsBase = 0; //This is  how many neighbors if we have 4 A, 2 B, 0 C style.
	public int variationsTypeTotal = 0; //This is how many neighbors if we have 4A, 2 set a, 0 set b style.
	public int variationsNeededTotal = 0; //How many total variations we will need.
	//public static biomeCount = 8;



	public int GetTypePossibilities ()
	{
		int returnint = 1;

	
		//	variationCombinations = returnint;
		if (variationArray.Count == 1 ) { variationsBase = 1; variationsTypeTotal = 1;  return 1; }
		if (variationArray.Count == 2 ) {
			returnint = 1;
			//variationCombinations = 
		//	Debug.Log( "Basic factorio of : " + variationArray[0] + " is : " + BasicFactorio( variationArray[0] ) );
			returnint = ( BasicFactorio( 6 ) / ( BasicFactorio( variationArray[0] - 1 ) * BasicFactorio( variationArray[1] ) ) );
			variationsBase = returnint;
			variationsTypeTotal = variationsBase * 6; //We look at this as 6 different base types can neighbor this.
			return returnint;

		}
		if ( variationArray.Count == 3 ) {
			returnint = 1;
			returnint = ( BasicFactorio( 6 ) / ( BasicFactorio( variationArray[0] - 1 ) * BasicFactorio( variationArray[1] ) * BasicFactorio( variationArray[2] ) ) );
			variationsBase = returnint;
			variationsTypeTotal = variationsBase * ( 6 * 5 ); //Set a has 6 things, set b has 5 things.
			return returnint;

		}
		return 0;

	}
	public static int BasicFactorio(int input )
	{
		int returnInt = 1;
		for (int i = 0 ; i < input ; ++i ) {
			returnInt *= input - i;
		}
		return returnInt;
	}

}