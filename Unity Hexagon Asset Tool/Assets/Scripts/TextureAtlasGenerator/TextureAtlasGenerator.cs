﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TextureAtlasGenerator {

	//texture array
	//This is a list of lists.  


	//UTILITY : EXPORT CONFIG - contains information here.  Overwrites file, no need to load it here.

	//each base type has only variations?  
	//Actual position info can be generated from this
	//IE 0 : 11     means 0 (full) has 11 variations to it.

	int exportResolution = 2048;
	int tileSize = 128;
	//float tileScale = 1.0f;
	string exportName = "SimpleExportTexture";
	int tilesAdded = 0;
	public Texture2D lastExportTexture = null;

	List<List<Texture2D>> baseTypeList = new List<List<Texture2D>>();

	public TextureAtlasGenerator ( int pRes , int pTile  , string pName )
	{
		exportResolution = pRes;
		tileSize = pTile;
		//tileScale = pTile;
		//tileScale = pScale;
		exportName = pName;
		//Add a list for each base type.  Assumes a hard 64.
		baseTypeList = new List<List<Texture2D>>( );
		for ( int i = 0 ; i < 65 ; ++i ) {
			baseTypeList.Add( new List<Texture2D>( ) );
		}
	}

	public void AddTextureToAtlas ( int baseType , Texture2D newTexture )
	{
		if ( baseType >= baseTypeList.Count ) { Debug.LogError( "ERROR: baseType : " + baseType + "  Exceeds texture atlas types" ); return; }
		tilesAdded++;
		baseTypeList[baseType].Add( newTexture );
	}
	public void FillAtlasWithSingle ()
	{
		for (int i = 1 ; i < baseTypeList.Count ; ++i ) {
			baseTypeList[i].Add( baseTypeList[0][0] );
		}
	}
	public Texture2D GenerateAtlas ()
	{
		if ( tilesAdded > Mathf.Pow( ( exportResolution / tileSize ) , 2 ) ) {
			Debug.LogError( "ERROR : Too many tiles.  Tiles added :" + tilesAdded + "  max size : " + Mathf.Pow( ( exportResolution / tileSize ) , 2 ) );
			return null;
		}
		Texture2D exportTexture = new Texture2D( exportResolution , exportResolution ,TextureFormat.RGBA32, false);
		exportTexture.filterMode = FilterMode.Point;
		Color32[] returnColor = exportTexture.GetPixels32();
		Color32[] tileColor;
		int currentTileX = 0;
		int currentTileY = 0;
	//	Debug.Log( "Generating texture" );
		foreach ( List<Texture2D> texList in baseTypeList ) {
			foreach ( Texture2D tex in texList ) {
			//	Debug.Log( "Starting new tex x : " + currentTileX + " y " + currentTileY);
				tileColor = tex.GetPixels32( );
				for ( int x = 0 ; x < tileSize ; ++x ) {
					for ( int y = 0 ; y < tileSize ; ++y ) {
						returnColor[( currentTileY*tileSize * exportResolution + currentTileX*tileSize ) + ( y * exportResolution + x )] = tileColor[y * tileSize + x];
					}
				}

				currentTileX++;
				if ( currentTileX >= ( exportResolution / tileSize ) ) {
					currentTileX = 0;
					currentTileY++;
				}

			}
		}
		exportTexture.SetPixels32( returnColor );
		exportTexture.Apply( );
		lastExportTexture = exportTexture;
		return exportTexture;
	}
	public void GenerateAtlasToFile ()
	{
		Texture2D exportTexture = GenerateAtlas( );


		string fullPath = "Assets/TextureExports/" + exportName + ".png";
		File.WriteAllBytes( fullPath , exportTexture.EncodeToPNG( ) );
	}

	//--Returns a vector2 value (0.0 to 1.0) of the bottom left tile size to top right.   
	//If variationCount is -1, than it will pick a random variation to use.
	public Vector4 GetUVTextureForType(int baseType , int variationCount )
	{
		Vector4 returnVector = new Vector4();
		int baseTileStart = 0;
		if ( baseType >= baseTypeList.Count ) { Debug.LogError( "ERROR: baseType too high!" ); return new Vector4(); }
		if ( variationCount >= 0 && variationCount >= baseTypeList[baseType].Count ) { Debug.LogError( "ERROR: variationType too high!" ); return new Vector4(); }
		//Count the tiles we need to offset by.
		for (int i = 0 ; i < baseType ; ++i ) {
			baseTileStart += baseTypeList[i].Count;
		//	if ( baseType == 63 ) { Debug.Log( "adding... : " + i + "  amount : " + baseTypeList[i].Count ); }

		}
		if (variationCount < 0 ) { variationCount = Random.Range( 0 , baseTypeList[baseType].Count-1 ); }
		baseTileStart += variationCount;
		
		int tileBaseY = Mathf.FloorToInt(baseTileStart / (exportResolution / tileSize));
		int tileBaseX = baseTileStart%(exportResolution / tileSize);

		//if ( baseType == 63 ) { Debug.Log( "63tileBaseX : " + tileBaseX ); }
		//if ( baseType == 63 ) { Debug.Log( "63tileBaseY : " + tileBaseY ); }
		//if ( baseType == 63 ) { Debug.Log( "63baseTileStart : " + baseTileStart ); }
		//if ( baseType == 63 ) { Debug.Log( "63variationCount : " + variationCount ); }

		//if ( baseType == 0 ) { Debug.Log( "0tileBaseX : " + tileBaseX ); }
		//if ( baseType == 0 ) { Debug.Log( "0tileBaseY : " + tileBaseY ); }
		returnVector.x = tileBaseX * ( 1.0f / ( exportResolution / tileSize ) );
		returnVector.y = tileBaseY * ( 1.0f / ( exportResolution / tileSize ) );
		returnVector.z = ( tileBaseX + 1.0f) * ( 1.0f / ( exportResolution / tileSize ) );
		returnVector.w =( tileBaseY+1.0f) * ( 1.0f / ( exportResolution / tileSize ) );

		return returnVector;
	}
}
