﻿using System.IO;
using UnityEngine;
[ExecuteInEditMode]
public class CameraExportFromRenderTexture : MonoBehaviour {

	RenderTexture renderTexture;
	public string prefixName;
	public string path;
	public bool takeSnapshot = false;
	void Update ()
	{
		if ( !takeSnapshot ) { return; }
		takeSnapshot = false;
		//ExportToPng( "TestImage.png" );
	}

	
	public Texture2D ExportToPng ( string inputName, int imageRes )
	{
		Texture2D render = GetTexture(imageRes );
		string fullPath = path + prefixName + inputName;
		File.WriteAllBytes( fullPath , render.EncodeToPNG( ) );
		return render;
	}

	public void ExportToPng ( Texture2D render , string inputName )
	{
		string fullPath = path + prefixName + inputName;
		File.WriteAllBytes( fullPath , render.EncodeToPNG( ) );
		//return render;
	}

	//Exports directly to path itself
	public Texture2D ExportToPngPath ( string inputName, int imageRes )
	{
		Texture2D render = GetTexture(imageRes );
		string fullPath = inputName;
		File.WriteAllBytes( fullPath , render.EncodeToPNG( ) );
		return render;
	}

	public void ExportToPngPath ( Texture2D render , string inputName )
	{
		string fullPath = inputName;
		File.WriteAllBytes( fullPath , render.EncodeToPNG( ) );
		//return render;
	}

	public Texture2D GetTexture (int imageRes)
	{
		Texture2D render = RTImage(GetComponent<Camera>(), imageRes);
		render.Apply( );
		return render;
	}


	Texture2D RTImage ( Camera cam, int imageRes )
	{
		RenderTexture currentRT = cam.activeTexture;
		RenderTexture targetRT = cam.targetTexture;
		if (cam.targetTexture.width != imageRes || cam.targetTexture.height != imageRes ) {
			cam.targetTexture.Release( );
			targetRT.width = imageRes;
			targetRT.height = imageRes;
			cam.targetTexture = targetRT;
		}
		RenderTexture.active = cam.targetTexture;






		cam.Render( );
		Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height, TextureFormat.RGBA32, false);
		
		image.ReadPixels( new Rect( 0 , 0 , cam.targetTexture.width , cam.targetTexture.height ) , 0 , 0 );
		image.Apply( );
		//	RenderTexture.active = currentRT;
		return image;
	}


	//--This does nothing
	public static Texture2D ReapplyTransparency (   Texture2D returnTexture )
	{
	//	Color32[] alphaColor = alphaTexture.GetPixels32();
		Color32[] returnColor = returnTexture.GetPixels32();
		int colorTotal = 0;
		float colorMult = 0.0f;
		float alphaValueMult = 0.0f;
		Color32 fullColor;
		for ( int i = 0 ; i < returnColor.Length ; ++i ) {
			//	if ( alphaColor[i].a < 225 ) { returnColor[i].a = 0; } else { returnColor[i].a = 255; } 
			//The more alpha it is, the more I can normalize it.

			 

			/*
		
			centerColor[i].r = ( byte )( ( ( 1.0f - baseValueMult ) * centerColor[i].r ) + ( baseValueMult * allColor[i].r ) );
			centerColor[i].g = ( byte )( ( ( 1.0f - baseValueMult ) * centerColor[i].g ) + ( baseValueMult * allColor[i].g ) );
			centerColor[i].b = ( byte )( ( ( 1.0f - baseValueMult ) * centerColor[i].b ) + ( baseValueMult * allColor[i].b ) );
			*/
			 
			//--Counteract black background.  
			//--Create fullColor, a normalized color. 
			fullColor = new Color32( );
			colorTotal = returnColor[i].r + returnColor[i].g + returnColor[i].b;
			colorMult = 765 / ( 1 + colorTotal );
			fullColor.r = ( byte )( Mathf.Min(255, returnColor[i].r * colorMult ));
			fullColor.g = ( byte )( Mathf.Min( 255 , returnColor[i].g * colorMult ) );
			fullColor.b = ( byte )( Mathf.Min( 255 , returnColor[i].b * colorMult ) );
			//--

			//--alphaValueMult is 0 at full transparency.  1.0 at full opaque.  Used to mix colors.
			alphaValueMult = 1.0f - ( returnColor[i].a / 255 );
			//--Set the colors.  The more transparent the pixel is, the more normalized it will become.
			returnColor[i].r = ( byte )( ( ( 1.0f - alphaValueMult ) * returnColor[i].r ) + ( alphaValueMult * fullColor.r ) );
			returnColor[i].g = ( byte )( ( ( 1.0f - alphaValueMult ) * returnColor[i].g ) + ( alphaValueMult * fullColor.g ) );
			returnColor[i].b = ( byte )( ( ( 1.0f - alphaValueMult ) * returnColor[i].b ) + ( alphaValueMult * fullColor.b ) );
			//--


			if ( returnColor[i].a < 255 ) { returnColor[i].a = ( byte )( Mathf.Clamp( ((returnColor[i].a-60) * 8 ),0,255 )); }
			//	returnColor[i].a = alphaColor[i].a;

		}
		returnTexture.SetPixels32( returnColor );
		returnTexture.Apply( );
		return returnTexture;
	}

	public static Texture2D FrontColorToBackgroundAlpha ( Texture2D centerHexagonColor , Texture2D allNeighborHexagon )
	{
	//	Color32[] alphaColor = alphaTexture.GetPixels32(); //A mask samlpling.  0 or 255.
		Color32[] centerColor = centerHexagonColor.GetPixels32(); //What we return, and what mostly what we want.  Due to AA, as it approachess the edge it can blend with black pixels.  
		Color32[] allColor = allNeighborHexagon.GetPixels32(); //So if centerColor is slightly transparent, we fill it in with color from this. 
		float baseValueMult = 0.0f;
		for ( int i = 0 ; i < centerColor.Length ; ++i ) {
			//if (centerColor[i].r != 0 || centerColor[i].b != 0 || centerColor[i].g != 0 ) { centerColor[i].a = 255; }
			//	else { centerColor[i].a = 0; }
			if ( centerColor[i].a >= 0 && centerColor[i].a < 255 ) {
				baseValueMult = 1.0f - ( centerColor[i].a / 255 );
				centerColor[i].r =(byte) (((1.0f - baseValueMult ) * centerColor[i].r) + (baseValueMult * allColor[i].r));
				centerColor[i].g = ( byte )( ( ( 1.0f - baseValueMult ) * centerColor[i].g ) + ( baseValueMult * allColor[i].g ) );
				centerColor[i].b = ( byte )( ( ( 1.0f - baseValueMult ) * centerColor[i].b ) + ( baseValueMult * allColor[i].b ) );
			//	centerColor[i].a = 255;
			}
			if (centerColor[i].g == 0 && centerColor[i].b == 0 && centerColor[i].r == 0) {
			//	centerColor[i].r = allColor[i].r;
			//	centerColor[i].g = allColor[i].g;
			//	centerColor[i].b = allColor[i].b;
			}
		}

		//for (int i = 0 ; i < alphaColor.Length ; ++i ) {
		//	if (centerColor[i].a < 254 && centerColor[i].a >0) { centerColor[i] = allColor[i]; centerColor[i].a = 255;}
		//	if (centerColor[i].a > 0 && centerColor[i].a != 255 ) { centerColor[i].a = 255; centerColor[i].b = 255; }
		//	//if ( alphaColor[i].a >0 && alphaColor[i].a < 255) {
		//	//	frontColor[i] = backColor[i];
		//	//	frontColor[i].a = 255;
		//	//}
		//	//if (alphaColor[i].a == 0 ) { frontColor[i].r = 0; }
		//}
		centerHexagonColor.SetPixels32( centerColor );
		centerHexagonColor.Apply( );


		return centerHexagonColor;//
	}

	//Checks the single pixel border.  If any aren't fully transparent, return false.
	public static bool CheckIfSidesAreTransparent ( Texture2D inputTexture )
	{
		for ( int y = 0 ; y < inputTexture.height ; ++y ) {
			for ( int x = 0 ; x < inputTexture.width ; ++x ) {
				if ( x == 0 ) {
					if ( inputTexture.GetPixel( x , y ).a == 1.0f ) { Debug.Log( "Side not transparent LH: " ); return false; }
				}


				if ( x == inputTexture.width - 1 ) {
					if ( inputTexture.GetPixel( x , y ).a == 1.0f ) { Debug.Log( "Side not transparent RH: " ); return false; }

				}

				//I do not care if the bottom is not transparent - the previous hexagon will cover any issues up.
			//	if ( y == 0 ) {
			//		if ( inputTexture.GetPixel( x , y ).a == 1.0f ) { Debug.Log( "Side not transparent BOTTOM: " ); return false; }

			//	}

				if ( y == inputTexture.height - 1 ) {
					if ( inputTexture.GetPixel( x , y ).a == 1.0f ) { Debug.Log( "Side not transparent TOP: " ); return false; }
				}

			}
		}
		return true;
	}

	public static Texture2D AddTransparencyToSides ( Texture2D inputTexture )
	{
		Color32[] baseColor = inputTexture.GetPixels32(); //A mask samlpling.  0 or 255.
		for ( int y = 0 ; y < inputTexture.height ; ++y ) {
			for ( int x = 0 ; x < inputTexture.width ; ++x ) {
				if ( x < 4 ) {
					
				//	baseColor[y * inputTexture.width + x].a -= ( byte )( 40 * ( 3 - x ) );
					baseColor[y * inputTexture.width + x].a = (byte) (Mathf.Max( baseColor[y * inputTexture.width + x].a * x/3.0f , 0 ));
				}

				if ( x > inputTexture.width - 5 ) {
					baseColor[y * inputTexture.width + x].a = ( byte )( Mathf.Max( baseColor[y * inputTexture.width + x].a * ( inputTexture.width - x - 1 )/3.0f  , 0 ) );// - ( 40 * ( 3 - (inputTexture.width - x - 1 ) ))

					//baseColor[y * inputTexture.width + x].a -= ( byte )( 40 * ( 3 - inputTexture.width - x - 1 ) );
				}

				if ( y < 4 ) {

					//baseColor[y * inputTexture.width + x].a -= ( byte )( 40 * ( 3 - y ) );
					baseColor[y * inputTexture.width + x].a = ( byte )( Mathf.Max( baseColor[y * inputTexture.width + x].a * y / 3.0f , 0 ) );

				}
				if ( y > inputTexture.height - 5 ) {

					//baseColor[y * inputTexture.width + x].a -= ( byte )( 40 * ( 3 - inputTexture.height - y - 1 ) );
					baseColor[y * inputTexture.width + x].a = ( byte )( Mathf.Max( baseColor[y * inputTexture.width + x].a * ( inputTexture.height - y - 1 ) / 3.0f , 0 ) );

				}

			}
		}
		inputTexture.SetPixels32( baseColor );
		inputTexture.Apply( );


		return inputTexture;//
	}
}
