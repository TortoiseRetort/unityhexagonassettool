﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class CameraShowSize : MonoBehaviour {
	public Camera renderCamera;
	public float cameraSize = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if ( !renderCamera ) { return; }
		float width = renderCamera.ViewportToWorldPoint(new Vector3(1,0,0)).x - renderCamera.ViewportToWorldPoint(new Vector3(0,0,0)).x;
		cameraSize = width;
	//	float height = renderCamera.ViewportToWorldPoint( new Vector3( 0 , 0 , 1 ) ).y - renderCamera.ViewportToWorldPoint( new Vector3( 0 , 0 , 0 ) ).y;
	//	Debug.Log( "Camera plane width : " + width );
	//	Debug.Log( "Camera plane height : " + height );
	}

}
