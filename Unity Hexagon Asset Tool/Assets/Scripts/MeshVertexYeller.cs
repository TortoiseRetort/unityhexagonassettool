﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MeshVertexYeller : MonoBehaviour {
	public void Update ()
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		Vector2[] uvs = mesh.uv;
		Vector3[] verts = mesh.vertices;
		foreach ( Vector2 vert in verts ) {
			Debug.Log( "vert : " + vert );
		}
		foreach (Vector2 uv in uvs ) {
			Debug.Log( "UV : " + uv );
		}
		Debug.Log( "---" );
	}
}
