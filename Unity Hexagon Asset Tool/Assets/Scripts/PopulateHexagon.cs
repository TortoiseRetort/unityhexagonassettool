﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]

public class PopulateHexagon : MonoBehaviour {
	public bool populate = false;
	public MeshFilter populateMesh;
	public LayerMask layerMask;

	//--These will be moved to their own object eventually.
	public Transform populateObject;
	public float rotateAngle = 15.0f;
	public float minDistance = 3.0f;
	public List<Transform> spawnedList = new List<Transform>(16);
	// Use this for initialization


	// Update is called once per frame
	void Update () {
		if ( !populate ) { return; }
		if (populateMesh == null ) { return; }
		populate = false;
		RefreshObject( );

	}
	//
	public void RefreshObject ()
	{
		populateMesh = GetComponent<MeshFilter>( );

		if ( spawnedList != null ) { DeleteTransforms( spawnedList ); }
		spawnedList = new List<Transform>( 32 );
		int counter = 0;
		int maxCounter = 0;
		while ( counter < 50 && maxCounter++ < 50000 ) {
			if ( CreateObjectOnMesh( ) == false ) { counter++; }
			else { counter = 0; }
		}
	}
	bool CreateObjectOnMesh ()
	{
		//Try to find a point that is allowed.  
		Vector3 tryPoint = RandomPointOnMesh.GetRandomPointOnMesh(populateMesh.sharedMesh);
		tryPoint += new Vector3( Random.Range( -1.0f , 1.0f ) , Random.Range( -1.0f , 1.0f ), 0.0f );
		//	tryPoint = new Vector3( tryPoint.x , tryPoint.z , tryPoint.y );
		RaycastHit hit;
		// Does the ray intersect any objects excluding the player layer
		Vector3 rayTryPoint = transform.position + tryPoint;
		rayTryPoint = new Vector3( rayTryPoint.x , rayTryPoint.z , rayTryPoint.y );


		if ( Physics.Raycast( rayTryPoint + new Vector3(0,10,0) ,( Vector3.down ) , out hit , Mathf.Infinity , layerMask ) ) {
		//	Debug.Log( "hit : " + hit.transform.name );
			return hit.transform.GetComponent<PopulateHexagon>().AttemptMeshCreate( hit.point );
		}
		return false;
	//	return AttemptMeshCreate( tryPoint);
	}
	//Get correct mesh here

	//Handle mesh creatoin here
	public bool AttemptMeshCreate(Vector3 tryPoint )
	{
		//
	//	tryPoint = new Vector3( tryPoint.x , tryPoint.z , tryPoint.y );
		if ( !IsPointValid( tryPoint , spawnedList , minDistance ) ) {
			 
			return false;
		}
		//The point is valid.
		CreateObject( tryPoint );
		return true;
	}

	public void CreateObject (Vector3 spawnPoint)
	{
		Transform newSpawn = Instantiate(populateObject);
		spawnedList.Add( newSpawn );
	//	spawnPoint = new Ve
		newSpawn.Rotate( new Vector3( rotateAngle , 0.0f , 0.0f ) );
		newSpawn.Rotate( new Vector3( 0.0f , Random.Range( 0.0f , 360.0f ) , 0.0f ) );
		newSpawn.Rotate( new Vector3( 0.0f , 0.0f , Random.Range( -7.5f , 7.5f ) ) );

		newSpawn.parent = transform;
		newSpawn.position = spawnPoint;
		newSpawn.localScale *= Random.Range( 0.9f , 1.1f );
		newSpawn.localScale *= Random.Range( 0.9f , 1.1f );
		newSpawn.localScale *= Random.Range( 0.9f , 1.1f );
		newSpawn.localScale *= Random.Range( 0.9f , 1.1f );

	}

	bool IsPointValid(Vector3 checkPoint, List<Transform> checkList, float minDist )
	{
		float checkDistance = 0.0f;
		
		foreach (Transform checkT in checkList ) {
			if ( checkT != null ) {
				checkDistance = Vector3.Distance( checkT.position , checkPoint );
				if ( checkDistance < minDist ) { return false; }
			}
		}
		return true;
	}
	void DeleteTransforms(List<Transform> deleteList )
	{
		for (int i = 0 ; i < deleteList.Count ; ++i ) {
			if ( deleteList[i] != null ) {
				DestroyImmediate( deleteList[i].gameObject );
			}
		}
	}
}
