﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RandomMaterialAssign : MonoBehaviour {
	public bool randomizeMaterials = false;
	public bool remakeTestSheet = false;

	public List<Material> materialList = new List<Material>();
	public List<Material> materialList_64 = new List<Material>(); //This material list includes 64 base types for initial testing.
	public Material baseMaterial;


	public Transform baseHexagon;
	public Vector2 areaSize = new Vector2(12,12);
	// Use this for initialization

	public void CleanMaterialList64 ()
	{
		foreach ( Material mat in materialList_64 ) {
			if ( mat.mainTexture != null ) { mat.mainTexture = null; }

		}
		materialList_64 = new List<Material>( );

	}
	

	// Update is called once per frame
	void Update ()
	{
		if ( randomizeMaterials ) { randomizeMaterials = false; ReassignMaterials( ); return; }
		if ( remakeTestSheet ) { remakeTestSheet = false; RemakeTestSheet( ); return; }
	//	if ( correctAssign ) { correctAssign = false; CorrectAssignMain( ); }
	}


	public void RemakeTestSheet ()
	{
		Transform[] children = GetComponentsInChildren<Transform>( );
		foreach ( Transform child in children ) { if ( child != null && child != transform ) { DestroyImmediate( child.gameObject ); } }
		Transform newObject;
		Vector3 createPoint = new Vector3(0,0,0);
		for ( int x = 0 ; x < areaSize.x ; ++x ) {
			for ( int y = 0 ; y < areaSize.y ; ++y ) {
				newObject = Instantiate( baseHexagon , transform );
				createPoint.x = 1.0f * x;
				createPoint.z = 1.0f * y;
				if ( x % 2 == 1 ) { createPoint.z -= 0.5f; }
				createPoint.y = createPoint.z / areaSize.y * 0.001f;
				createPoint.z *= -1.0f;
				newObject.localPosition = createPoint;

			}
		}
		ReassignMaterials( );
	}
	public void ReassignMaterials ()
	{
		MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
		foreach ( MeshRenderer meshR in meshRenderers ) {
			meshR.sharedMaterial = materialList[Random.Range( 0 , materialList.Count - 1 )];
		}
	}
	public void SimpleAssignFull ( TextureAtlasGenerator textureAtlas )
	{
		//This assigns everything as if they were a full blank
		if ( areaSize.x < 12 ) { areaSize.x = 12; }
		if ( areaSize.y < 12 ) { areaSize.y = 12; }
		Transform[] children = GetComponentsInChildren<Transform>( );
		foreach ( Transform child in children ) { if ( child != null && child != transform ) { DestroyImmediate( child.gameObject ); } }
		List<Transform> childList = new List<Transform>();
		Transform newObject;
		Vector3 createPoint = new Vector3(0,0,0);
		baseHexagon.GetComponent<MeshRenderer>( ).sharedMaterial.mainTexture = textureAtlas.lastExportTexture;

		for ( int x = 0 ; x < ( int )areaSize.x ; ++x ) {
			for ( int y = 0 ; y < ( int )areaSize.y ; ++y ) {
				newObject = Instantiate( baseHexagon , transform );
				newObject.SetParent( transform );
				newObject.name = "tile  " + x + "|" + y + "   ";
				childList.Add( newObject );
				createPoint.x = 1.0f * x;
				createPoint.z = 1.0f * y;
				if ( x % 2 == 1 ) { createPoint.z -= 0.5f; }
				//createPoint.y = createPoint.z / areaSize.y * 0.02f;
				createPoint.y = -( createPoint.z / areaSize.y ) * 0.02f;
				//	createPoint.z *= -1.0f;
				newObject.localPosition = createPoint;

			}
		}


		List<int> basicTileTypes = new List<int>();
		//Create random noise for the main map
		for ( int x = 0 ; x < areaSize.x ; ++x ) {
			for ( int y = 0 ; y < areaSize.y ; ++y ) {
				basicTileTypes.Add( 1 );
			}
		}

		int positionID = 0;
		int baseType = 0;

		for ( int x = 0 ; x < areaSize.x ; ++x ) {
			for ( int y = 0 ; y < areaSize.y ; ++y ) {
				positionID = GetPositionID( x , y );
				baseType = 0;
				SetTileUVToType( childList[positionID] , 64 , textureAtlas );
				childList[positionID].name += "full";

			}

		}
	}
	public void CorrectAssignMain ( TextureAtlasGenerator textureAtlas)
	{
		if ( areaSize.x < 12 ) { areaSize.x = 12; }
		if ( areaSize.y < 12 ) { areaSize.y = 12; }
		Transform[] children = GetComponentsInChildren<Transform>( );
		foreach ( Transform child in children ) { if ( child != null && child != transform ) { DestroyImmediate( child.gameObject ); } }
		List<Transform> childList = new List<Transform>();
		Transform newObject;
		Vector3 createPoint = new Vector3(0,0,0);
		baseHexagon.GetComponent<MeshRenderer>( ).sharedMaterial.mainTexture = textureAtlas.lastExportTexture;
		for ( int x = 0 ; x < ( int )areaSize.x ; ++x ) {
			for ( int y = 0 ; y < ( int )areaSize.y ; ++y ) {
				newObject = Instantiate( baseHexagon , transform );
				newObject.SetParent( transform );
				newObject.name = "tile  " + x + "|" + y + "   ";
				childList.Add( newObject );
				createPoint.x = 1.0f * x;
				createPoint.z = 1.0f * y;
				if ( x % 2 == 1 ) { createPoint.z -= 0.5f; }
				//createPoint.y = createPoint.z / areaSize.y * 0.02f;
				createPoint.y = -( createPoint.z / areaSize.y ) * 0.02f;
				//	createPoint.z *= -1.0f;
				newObject.localPosition = createPoint;

			}
		}

		List<int> basicTileTypes = new List<int>();
		//Create random noise for the main map
		for ( int x = 0 ; x < areaSize.x ; ++x ) {
			for ( int y = 0 ; y < areaSize.y ; ++y ) {
				if ( Random.Range( 0.0f , 1.0f ) <= 0.6f ) {
					basicTileTypes.Add( 0 );
				}
				else { basicTileTypes.Add( 1); }
			}
		}

		int positionID = 0;
		int baseType = 0;

		for ( int x = 0 ; x < 7 ; ++x ) {
			for ( int y = 0 ; y < 7 ; ++y ) {
				positionID = GetPositionID( x , y );
				basicTileTypes[positionID] = 0;
			}
		}
		positionID = GetPositionID( 3 , 3 );
		basicTileTypes[positionID] = 1;


		for ( int x = 0 ; x < areaSize.x ; ++x ) {
			for ( int y = 0 ; y < areaSize.y ; ++y ) {
				positionID = GetPositionID( x , y );
				baseType = 0;
				if ( basicTileTypes[positionID] == 0 ) {
					//Debug.Log( "Tile : " + x + " | " + y + " " );
					if ( basicTileTypes[GetOffsetID( x , y , 0 , 1 )] == 1 ) {
						baseType += ( int )Mathf.Pow( 2 , 0 );
						childList[positionID].name += ":1";
					} //TOP
					else {
						childList[positionID].name += ":0";
					}

					if ( basicTileTypes[GetOffsetID( x , y , 1 , 1 )] == 1 ) {
						baseType += ( int )Mathf.Pow( 2 , 5 );
						childList[positionID].name += ":1";
					} //TOP
					else {
						childList[positionID].name += ":0";
					}

					if ( basicTileTypes[GetOffsetID( x , y , 1 , 0 )] == 1 ) {
						baseType += ( int )Mathf.Pow( 2 , 4 );
						childList[positionID].name += ":1";
					} //TOP
					else {
						childList[positionID].name += ":0";
					}

					if ( basicTileTypes[GetOffsetID( x , y , 0 , -1 )] == 1 ) {
						baseType += ( int )Mathf.Pow( 2 , 3 );
						childList[positionID].name += ":1";
					} //TOP
					else {
						childList[positionID].name += ":0";
					}

					if ( basicTileTypes[GetOffsetID( x , y , -1 , 0 )] == 1 ) {
						baseType += ( int )Mathf.Pow( 2 , 2 );
						childList[positionID].name += ":1";
					} //TOP
					else {
						childList[positionID].name += ":0";
					}

					if ( basicTileTypes[GetOffsetID( x , y , -1 , 1 )] == 1 ) {
						baseType += ( int )Mathf.Pow( 2 , 1 );

						childList[positionID].name += ":1";
					} //TOP
					else {
						childList[( positionID )].name += ":0";
					}
					
					SetTileUVToType( childList[positionID] , baseType , textureAtlas );
		//			childList[positionID].GetComponent<MeshRenderer>( ).sharedMaterial = materialList_64[( 63 - baseType ) * 1 + Random.Range( 0 , 0 )];
				}//
				else {
					SetTileUVToType( childList[positionID] , 64 , textureAtlas );
					//childList[positionID].GetComponent<MeshRenderer>( ).sharedMaterial = materialList_64[Random.Range( 0 , 0 )];
					childList[positionID].name += "full";
					//////////////////////	childList[positionID].name = "" + 64;

				}

			}

		}

	}
	int GetPositionID ( int posX , int posY )
	{
		if ( posX < 0 ) { posX = 0; }
		if ( posY < 0 ) { posY = 0; }
		if ( posX >= areaSize.x ) { posX = ( int )areaSize.x - 1; }
		if ( posY >= areaSize.y ) { posY = ( int )areaSize.y - 1; }
		return posX * ( int )areaSize.y + posY;
	}

	//I don't htink I need this
	int GetOffsetID ( int posX , int posY , int offsetX , int offsetY )
	{
		int yMod = 0;
		if ( posX % 2 == 1 ) {
			if ( offsetX != 0 ) { posX += offsetX; posY -= 1; }
		}
		else { posX += offsetX; }
		posY += offsetY;
		return GetPositionID( posX , posY );
		return 0;
	}

	void SetTileUVToType ( Transform modifyObject , int baseType , TextureAtlasGenerator textureAtlas )
	{
		Mesh mesh = Instantiate( modifyObject.GetComponent<MeshFilter>().sharedMesh);
		Vector2[] uvs = new Vector2[mesh.vertices.Length];
		Vector4 uvOffset = textureAtlas.GetUVTextureForType(baseType, -1);
		/*
		uvs[0].x = uvOffset.x; uvs[0].y = uvOffset.y;
		uvs[3].x = uvOffset.z; uvs[3].y = uvOffset.w;
		uvs[2].x = uvOffset.z; uvs[2].y = uvOffset.y;
		uvs[1].x = uvOffset.x; uvs[1].y = uvOffset.w;
		*/
		
		uvs[0].x = uvOffset.x; uvs[0].y = uvOffset.y;
		uvs[1].x = uvOffset.z; uvs[1].y = uvOffset.y;
		uvs[2].x = uvOffset.x; uvs[2].y = uvOffset.w;
		uvs[3].x = uvOffset.z; uvs[3].y = uvOffset.w;
		 
 

		mesh.uv = uvs;
		modifyObject.GetComponent<MeshFilter>().mesh = mesh;
	}
}
